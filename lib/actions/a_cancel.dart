import 'a_action.dart';
import 'package:flutter/material.dart';

class AActionCancel extends AAction {
  const AActionCancel({
    super.key,
    required super.onAction,
    super.loading,
    super.disabled,
  }) : super(
    text: "Cancelar",
    style: const AActionStyle(
      backgroundColor: Colors.transparent,
      textColor: Colors.black,
      borderColor: Colors.transparent,
    ),
  );
}
