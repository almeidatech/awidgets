import 'package:flutter/material.dart';
import 'a_action.dart';

class AActionDelete extends AAction {
  const AActionDelete({
    super.key,
    required super.onAction,
    super.loading,
    super.disabled,
    super.alwaysShow,
  }) : super(
    text: "Deletar",
    style: const AActionStyle(
      backgroundColor: Colors.red,
      textColor: Colors.white,
      borderColor: Colors.red,
    ),
  );
}
