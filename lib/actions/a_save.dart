import 'package:flutter/material.dart';
import 'a_action.dart';

class AActionSave extends AAction {
  AActionSave(BuildContext context, {
    super.key,
    required super.onAction,
    super.loading,
    super.disabled,
    super.alwaysShow,
  }) : super(
    text: "Salvar",
    style: AActionStyle(
      backgroundColor: Theme.of(context).primaryColor,
      textColor: Colors.white,
    ),
  );
}
