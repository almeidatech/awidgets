import 'package:awidgets/utils.dart';
import 'package:flutter/material.dart';

class AAction extends StatefulWidget {
  final Function onAction;
  final bool? loading;
  final bool? disabled;
  final String text;
  final AActionStyle? style;
  final bool? submitFormButton;
  final bool alwaysShow;
  const AAction({
    super.key,
    required this.onAction,
    this.loading,
    this.disabled,
    this.style,
    this.submitFormButton,
    required this.text,
    this.alwaysShow = false,
  });

  @override
  State<AAction> createState() => _AActionState();
}

class _AActionState extends State<AAction> {
  @override
  void initState() {
    super.initState();
  }

  final BorderRadius borderRadius = BorderRadius.circular(3);

  Color textColor() {
    if (widget.disabled == true) {
      return Colors.grey;
    }
    Color color = widget.style?.textColor ?? Theme.of(context).primaryColor;
    if (isDarkMode(context) && color.isDark) {
      return lighten(color, 0.6);
    }
    return color;
  }

  Widget _button(
    String text,
  ) {
    return InkWell(
      onTap: () async {
        if (widget.loading == true) {
          return;
        }
        if (widget.disabled == true) {
          return;
        } else {
          return widget.onAction();
        }
      },
      borderRadius: borderRadius,
      child: Container(
        alignment: Alignment.center,
        // padding: const EdgeInsets.all(8),
        padding: const EdgeInsets.symmetric(
          vertical: 12,
          horizontal: 14,
        ),
        decoration: BoxDecoration(
          borderRadius: borderRadius,
          border: Border.all(
            color: widget.disabled == true
                ? Colors.grey[300]!
                : widget.style?.borderColor ?? Theme.of(context).primaryColor,
          ),
          color: widget.disabled == true
              ? Colors.grey[300]
              : widget.style?.backgroundColor ?? Colors.transparent,
        ),
        child: widget.loading == true
            ? loadingWidget()
            : Text(
                text,
                style: TextStyle(
                  color: textColor(),
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                ),
              ),
      ),
    );
  }

  Widget loadingWidget() {
    return SizedBox(
      height: 20,
      width: 20,
      child: CircularProgressIndicator(
        strokeWidth: 2,
        color: widget.style?.textColor ?? Theme.of(context).primaryColor,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _button(widget.text.toUpperCase());
  }
}

class AActionStyle {
  final Color? borderColor;
  final Color? backgroundColor;
  final Color? textColor;

  const AActionStyle({
    this.borderColor,
    this.backgroundColor,
    this.textColor,
  });
}
