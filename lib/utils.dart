import 'package:decimal/decimal.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';


bool isDarkMode(BuildContext context) {
  return Theme.of(context).colorScheme.brightness == Brightness.dark;
}

extension ColorIsDark on Color {
  bool get isDark => computeLuminance() <= 0.179;
  bool get isLight => !isDark;
}

Color foregroundColorByLuminance(Color background) {
  if (background.isDark) {
    return Colors.white;
  } else {
    return Colors.grey.shade900;
  }
}

Color darken(Color color, [double amount = .1]) {
  assert(amount >= 0 && amount <= 1);

  final hsl = HSLColor.fromColor(color);
  final hslDark = hsl.withLightness((hsl.lightness - amount).clamp(0.0, 1.0));

  return hslDark.toColor();
}

Color lighten(Color color, [double amount = .1]) {
  assert(amount >= 0 && amount <= 1);

  final hsl = HSLColor.fromColor(color);
  final hslLight = hsl.withLightness((hsl.lightness + amount).clamp(0.0, 1.0));

  return hslLight.toColor();
}

DateTime? toDateTime(dynamic str) {
  if (str == null) {
    return null;
  }
  if (str is DateTime) {
    return str;
  }
  if (str is String) {
    if (str.isEmpty) {
      return null;
    }
    if (str.contains('/')) {
      List<String> parts = str.split(' ');
      if (parts.isEmpty) {
        return null;
      }
      str = parts[0].split('/').reversed.join('-');
      if (parts.length > 1) {
        for (String part in parts.sublist(1)) {
          if (str == null || str.isEmpty) {
            str = part;
          } else {
            str += " $part";
          }
        }
      }
    } else {
      str = str.replaceFirst('T', " ");
    }
    return DateTime.parse(str);
  }
  throw Exception("Unkown StringToDateTime type '$str'");
}

String formatCPF(String cpf) {
  if (cpf.length != 11) {
    return cpf;
  }
  return '${cpf.substring(0, 3)}.${cpf.substring(3, 6)}.${cpf.substring(6, 9)}-${cpf.substring(9, 11)}';
}

String removeDiacritics(String str) {
  String withDia =
      'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
  String withoutDia =
      'AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz';

  for (int i = 0; i < withDia.length; i++) {
    str = str.replaceAll(withDia[i], withoutDia[i]);
  }
  return str;
}

String? formatDate(DateTime? date, {bool? en}) {
  if (date == null) {
    return null;
  }
  if (en == true) {
    return DateFormat('MM/dd/yyyy').format(date);
  } else {
    return DateFormat('dd/MM/yyyy').format(date);
  }
}

String? formatTime(DateTime? date) {
  if (date == null) {
    return null;
  }
  return DateFormat('kk:mm').format(date);
}

String? formatDateTime(DateTime? date) {
  if (date == null) {
    return null;
  }
  return "${formatDate(date)} às ${formatTime(date)}";
}

String? formatCurrency({double? value, Decimal? decimalValue}) {
  if (value == null && decimalValue == null) {
    return null;
  }
  if (decimalValue != null) {
    value = decimalValue.toDouble();
  }
  return NumberFormat.currency(locale: 'pt_BR', symbol: 'R\$').format(value);
}

/// Use [withTime] to return python datetime format
String? dateToDB(DateTime? date, {bool withTime = false}) {
  if (date == null) {
    return null;
  }
  if (withTime) {
    return DateFormat('yyyy-MM-ddTHH:mm:ss.000').format(date);
  }
  return DateFormat('yyyy-MM-dd').format(date);
}

String? dateFormat(DateTime? date, {bool withDetails = false}) {
  if (date == null) {
    return null;
  }
  return DateFormat('dd/MM/yyyy${withDetails ? ' HH:mm:ss' : ''}').format(date);
}

String extractNumber(String text) {
  return text.replaceAll(RegExp(r'[^0-9]'), '');
}

String getLanguage(String localeName) {
  switch (localeName) {
    case 'pt_BR':
      return 'pt_BR';
    case 'en_US':
      return 'en';
    case 'es_ES':
      return 'es';
    default:
      return 'pt_BR';
  }
}

final RegExp _folderName = RegExp(r"[0-9a-zA-Z\_\-\. ]+$");

String? extractFolderName(String path) {
  if (path.endsWith('/') || path.endsWith('\\')) {
    path = path.substring(0, path.length - 1);
  }
  return _folderName.stringMatch(path);
}

class UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
        text: newValue.text.toUpperCase(), selection: newValue.selection);
  }
}
