import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';

import 'a_field_text.dart';

class AFieldNumber extends AFieldText {
  const AFieldNumber({
    super.key,
    super.onChanged,
    super.readOnly,
    super.required,
    super.autofillHints,
    super.icon,
    super.label,
    super.suffix,
    super.bottom,
    super.denySpaces,
    super.hintText,
    required super.identifier,
    super.initialValue = "",
    super.flexible,
    super.expanded,
    super.onUnfocus,
    super.margin,
    super.autofocus,
    super.capitalize,
    super.clearable,
    super.customRules,
    super.height,
    super.linkToAForm,
    super.maxLines,
    super.onSubmit,
    super.padding,
    super.readonly,
    super.timerTime,
  }) : super(
    keyboardType: TextInputType.number,
  );

  @override
  AFieldNumberState createState() => AFieldNumberState();
}

class AFieldNumberState<T extends AFieldNumber> extends AFieldTextState<T> {
  @override
  void initState() {
    super.initState();
    controller = MoneyMaskedTextController(
      thousandSeparator: '.',
      decimalSeparator: '',
      leftSymbol: "",
      initialValue: double.tryParse(widget.initialValue ?? "") ?? 0,
      precision: 0,
    );
  }
}
