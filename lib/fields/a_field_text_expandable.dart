import 'package:flutter/material.dart';
import 'a_field_text.dart';

/// AFieldTextExpandable
/// Extensão do AFieldText que permite expandir o campo de texto conforme o conteúdo digitado.
/// O maxLinex não tem efeito, pois o campo é expandido conforme o conteúdo.
/// Se desejar limitar o número de linhas, utilize o AFieldText.
class AFieldTextExpandable extends AFieldText {
  /// valor de multiplicação das linhas para calcular a altura do campo de texto (default: 10)
  final int lineMultiplier;

  /// Altura máxima do campo de texto (default: double.infinity)
  final double maxHeight;

  const AFieldTextExpandable({
    super.capitalize = false,
    super.keyboardType,
    super.clearable = false,
    super.maxLines = 1,
    super.hintText,
    super.autofocus = false,
    super.icon,
    super.suffix,
    super.bottom,
    super.autofillHints,
    super.onUnfocus,
    super.key,
    super.readOnly,
    required super.identifier,
    required super.label,
    super.required,
    super.customRules,
    super.denySpaces = false,
    super.height = 110,
    super.onChanged,
    super.initialValue,
    super.flexible,
    super.expanded,
    super.margin,
    super.linkToAForm,
    super.onSubmit,
    super.padding,
    super.readonly,
    this.lineMultiplier = 10,
    this.maxHeight = double.infinity,
    super.timerTime,
  });

  @override
  AFieldTextExpandableState createState() => AFieldTextExpandableState();
}

class AFieldTextExpandableState extends AFieldTextState<AFieldTextExpandable> {
  double textFieldHeight = 46;
  final GlobalKey _containerKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    controller.addListener(_adjustHeight);
    textFieldHeight = widget.height;
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _adjustHeight();
    });
  }

  void _adjustHeight() {
    final RenderBox? renderBox =
        _containerKey.currentContext?.findRenderObject() as RenderBox?;
    final double containerWidth = (renderBox?.size.width ?? 0);

    if (containerWidth == 0 || containerWidth < 0 || controller.text.isEmpty) {
      return;
    }

    final TextPainter textPainter = TextPainter(
      text: TextSpan(
        text: controller.text,
        style: TextStyle(
          color: widget.readOnly ? Colors.grey.shade800 : null,
        ),
      ),
      textDirection: TextDirection.ltr,
    );
    textPainter.layout(maxWidth: containerWidth);

    final int lines =
        (textPainter.size.height / textPainter.preferredLineHeight).ceil();
    final double height = textPainter.size.height;
    textFieldHeight = height +
        (widget.padding?.vertical ?? 0) +
        10 +
        (lines * widget.lineMultiplier); // padding + 10 extra + 2 * lines
    setState(() {
      textFieldHeight = textFieldHeight.clamp(widget.height, widget.maxHeight);
    });
  }

  @override
  void dispose() {
    controller.removeListener(_adjustHeight);
    super.dispose();
  }

  void setTimerTime(int? value) {
    super.timerTime = value ?? 500;
  }

  @override
  Widget textField() => Focus(
        focusNode: focusNode,
        onFocusChange: (bool haveFocus) {
          if (!haveFocus) {
            validate();
            widget.onUnfocus?.call();
          }
        },
        canRequestFocus: false,
        child: AnimatedContainer(
          duration: const Duration(milliseconds: 200),
          height: textFieldHeight,
          child: TextField(
            key: _containerKey,
            autofocus: widget.autofocus,
            readOnly: widget.readOnly,
            obscureText: obscure,
            maxLines: null,
            autocorrect: false,
            autofillHints: widget.autofillHints,
            textCapitalization: TextCapitalization.none,
            controller: controller,
            onChanged: onChanged,
            keyboardType: widget.keyboardType,
            inputFormatters: inputFormatters,
            style: TextStyle(
              color: widget.readOnly ? Colors.grey.shade800 : null,
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: const EdgeInsets.symmetric(horizontal: 8),
              isDense: true,
              labelText: labelText,
              hintText: widget.hintText,
              hintStyle: const TextStyle(
                fontWeight: FontWeight.w400,
              ),
              enabledBorder: InputBorder.none,
              focusedBorder: InputBorder.none,
              errorBorder: InputBorder.none,
              disabledBorder: InputBorder.none,
              isCollapsed: false,
              fillColor: null,
            ),
            onSubmitted: (_) => widget.onSubmit?.call(),
          ),
        ),
      );

  @override
  Widget childBuild() {
    return Container(
      constraints: BoxConstraints(minHeight: textFieldHeight),
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(4)),
        color: backgroundColor,
      ),
      padding: const EdgeInsets.symmetric(
        horizontal: 10,
      ),
      clipBehavior: Clip.hardEdge,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Column(
              children: <Widget>[
                const SizedBox(height: 5),
                textField(),
                const SizedBox(height: 2),
                errorText(),
                if (widget.bottom != null) widget.bottom!,
                const SizedBox(height: 5),
              ],
            ),
          ),
          ...suffixItems(),
        ],
      ),
    );
  }
}
