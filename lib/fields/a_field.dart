import 'dart:async';

import 'package:awidgets/general/a_form.dart';
import 'package:awidgets/utils.dart';
import 'package:flutter/material.dart';

typedef ARule<T> = String? Function(T? value);

abstract class AField<T> extends StatefulWidget {
  final String? label;
  final String identifier;
  final void Function(T?)? onChanged;
  final bool readOnly;
  final bool required;
  final bool readonly;
  final T? initialValue;
  final List<ARule<T>>? customRules;
  final bool expanded;
  final int? flexible;
  final EdgeInsets? padding;
  final EdgeInsets? margin;
  final bool linkToAForm;
  final VoidCallback? onSubmit;
  final int timerTime;
  const AField({
    super.key,
    required this.label,
    required this.identifier,
    this.initialValue,
    this.onChanged,
    this.expanded = false,
    this.flexible,
    this.readOnly = false,
    this.required = false,
    this.readonly = false,
    this.customRules,
    this.padding,
    this.margin,
    this.linkToAForm = true,
    this.onSubmit,
    this.timerTime = 500,
  });
}

abstract class AFieldState<TWidget extends AField<TValue>, TValue>
  extends State<TWidget>
{
  TValue? get value;
  set value(TValue? value);
  void clear();

  final List<ARule<TValue>> customRules = <ARule<TValue>>[];
  String? __errorText;

  Timer? _nextFuture;

  bool obscure = false;

  int timerTime = 500;

  // Color get backgroundColor => isDarkMode(context)
  //     ? Colors.grey.shade900
  //     : Colors.grey.shade200;

  Color get backgroundColor => (
      Theme.of(context).inputDecorationTheme.fillColor ??
      Theme.of(context).canvasColor
  );

  void setValue(TValue? value) {
    setState(() {
      this.value = value;
    });
  }

  @mustCallSuper
  void onChanged(TValue? value) {
    if (_nextFuture != null && _nextFuture!.isActive) {
      _nextFuture!.cancel();
    }
    _nextFuture = Timer.periodic(
      Duration(milliseconds: timerTime),
      (Timer timer) {
        widget.onChanged?.call(value);
        timer.cancel();
      },
    );
    validate();
  }

  @override
  void initState() {
    super.initState();
    if (widget.linkToAForm) {
      __linkAForm();
    }
    timerTime = widget.timerTime;
  }
  
  @override
  void didUpdateWidget(covariant TWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.linkToAForm != widget.linkToAForm) {
      if (widget.linkToAForm) {
        __linkAForm();
      } else {
        __unlinkAForm();
      }
    }
    timerTime = widget.timerTime;
  }

  void __linkAForm() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      AForm.maybeOf(context)?.addField(this);
    });
  }

  void __unlinkAForm() {
    AForm.maybeOf(context)?.removeField(this);
  }

  @override
  void deactivate() {
    if (widget.linkToAForm) {
      __unlinkAForm();
    }
    super.deactivate();
  }

  String? requiredRule<T>(T? value) {
    if (
      value == null ||
      (value is String && value.isEmpty) ||
      (value is bool && value == false) ||
      (value is int && value == 0)
    ) {
      return "Campo Obrigatório";
    }
    return null;
  }

  List<ARule<TValue>> get rules => <ARule<TValue>>[
    if (widget.required)
      requiredRule,
    ...customRules,
    if (widget.customRules != null)
      ...widget.customRules!,
  ];

  bool validate() {
    for (ARule<TValue> rule in rules) {
      String? error = rule.call(value);
      if (error != null) {
        Scrollable.ensureVisible(context);
        setState(() {
          __errorText = error;
        });
        return false;
      }
    }
    setState(() {
      __errorText = null;
    });
    return true;
  }

  Widget errorText({
    EdgeInsets padding = const EdgeInsets.only(top: 0),
  }) {
    return AnimatedSize(
      duration: const Duration(milliseconds: 100),
      child: __errorText == null
      ? const SizedBox(width: double.infinity)
      : Container(
        padding: padding,
        width: double.infinity,
        child: Text(
          __errorText!,
          style: const TextStyle(
            fontSize: 12,
            color: Colors.red,
          ),
          textAlign: TextAlign.start,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget child = childBuild();
    if (widget.margin != null) {
      child = Padding(
        padding: widget.margin!,
        child: child,
      );
    }
    if (widget.expanded) {
      return Expanded(
        flex: widget.flexible ?? 1,
        child: child,
      );
    }
    if (widget.flexible != null) {
      return Flexible(
        flex: widget.flexible!,
        child: child,
      );
    }
    return child;
  }

  Widget childBuild();

}
