import 'package:flutter/material.dart';
import 'a_field.dart';

class AFieldCheckbox extends AField<bool> {
  const AFieldCheckbox({
    super.onChanged,
    super.key,
    super.readOnly,
    super.required,
    super.initialValue,
    String super.label = '',
    required super.identifier,
    super.flexible,
    super.expanded,
    super.padding,
    super.margin,
    super.linkToAForm,
    super.onSubmit,
    super.timerTime,
  });

  @override
  AFieldCheckBoxState createState() => AFieldCheckBoxState();
}

class AFieldCheckBoxState extends AFieldState<AFieldCheckbox, bool> {
  @override
  bool? value = false;

  @override
  void initState() {
    super.initState();
    value = widget.initialValue;
  }

  void setValue(bool? value) {
    setState(() {
      this.value = value;
    });
  }

  @override
  void clear() {
    setState(() {
      value = null;
    });
  }

  @override
  Widget childBuild() {
    return InkWell(
      onTap: widget.readOnly
          ? null
          : () {
              setState(() {
                value = !(value ?? false);
              });
              widget.onChanged?.call(value);
            },
      child: Padding(
        padding: widget.padding ?? EdgeInsets.zero,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            FadeTransition(
              opacity: widget.readOnly
                  ? const AlwaysStoppedAnimation<double>(0.5)
                  : const AlwaysStoppedAnimation<double>(1),
              child: value == true
                  ? Icon(Icons.check_box, color: Theme.of(context).primaryColor)
                  : const Icon(Icons.check_box_outline_blank,
                      color: Colors.grey),
            ),
            const SizedBox(width: 8),
            Flexible(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  widget.label!,
                  style: TextStyle(
                    fontSize: 16,
                    color: widget.readOnly
                        ? Colors.grey
                        : Theme.of(context).textTheme.bodyLarge!.color,
                  ),
                ),
                errorText(),
              ],
            ))
          ],
        ),
      ),
    );
  }
}
