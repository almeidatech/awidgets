import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

import 'a_field_phone.dart';


class AFieldPhonePrefix extends AFieldPhone{
  final String prefix;
  const AFieldPhonePrefix({
    super.key,
    super.onChanged,
    super.readOnly,
    super.required,
    bool? showIcon,
    super.initialValue = "",
    super.label = "Telefone",
    super.identifier = 'phone',
    super.isArgentine = false,
    super.flexible,
    super.expanded,
    super.margin,
    super.autofocus,
    super.bottom,
    super.capitalize,
    super.clearable,
    super.customRules,
    super.denySpaces,
    super.height,
    super.linkToAForm,
    super.onSubmit,
    super.onUnfocus,
    super.padding,
    super.readonly,
    super.suffix,
    this.prefix = '',
    super.hintText,
    super.timerTime,
  }) : super();

  @override
  AFieldPhonePrefixState createState() => AFieldPhonePrefixState();

}


class AFieldPhonePrefixState extends AFieldPhoneState<AFieldPhonePrefix> {


  @override
  String? validatePhoneNumber(String? value) {
    if (value == null || value.isEmpty) {
      return null;
    }
    value = value.replaceFirst(widget.prefix, '');
    if (!{14, 15}.contains(value.length)) {
      return "Telefone Inválido";
    }
    return null;
  }

  String maskWithPrefix(String mask) {
    return widget.prefix + mask;
  }

  @override
  void initState() {
    super.initState();
    inputFormatters.add(
      MaskTextInputFormatter(
        mask: maskWithPrefix('(##) ####-#####'),
        filter: <String, RegExp>{'#': RegExp(r'[0-9]')},
      ),
    );
    verifyPhoneMask(widget.initialValue ?? '');
    if (kDebugMode) {
      print(customRules);
    }
    hintText = widget.hintText ?? (){
      '${widget.prefix}(00) 0000-0000';
    }();
    adaptInitialValue();
  }

  @override
  void setValue(String? value) {
    super.setValue(value);
    verifyPhoneMask(value ?? '');
  }

  @override
  String? phoneValidator() {
    if (value.trim().isEmpty) {
      return null;
    }
    if (widget.isArgentine) {
      if ((value.isNotEmpty) && value.length != 14) {
        return "Utilize um número de celular válido";
      } else {
        return null;
      }
    } else {
      if ((value.isNotEmpty) && (value.length < 14 || value.length > 15)) {
        return "Utilize um número de celular válido";
      } else {
        return null;
      }
    }
  }

  @override
  void verifyPhoneMask(String value) {
    String valueWithoutPrefix = value;
    valueWithoutPrefix = valueWithoutPrefix.replaceFirst(widget.prefix, '');
    if (valueWithoutPrefix.length > 14) {
      if (currentMask != maskWithPrefix('(##) #####-####')) {
        setState(() {
          currentMask = maskWithPrefix('(##) #####-####');
        });
        for (TextInputFormatter element in inputFormatters) {
          if (element is MaskTextInputFormatter) {
            element.updateMask(
              mask: maskWithPrefix('(##) #####-####'),
              filter: <String, RegExp>{'#': RegExp(r'[0-9]')},
              newValue: TextEditingValue.empty,
            );
            value = element
                .formatEditUpdate(
                TextEditingValue.empty, TextEditingValue(text: value))
                .text;
          }
        }
        setValue(value);
      }
    } else {
      if (currentMask != maskWithPrefix('(##) ####-#####')) {
        setState(() {
          currentMask = maskWithPrefix('(##) ####-#####');
        });
        for (TextInputFormatter element in inputFormatters) {
          if (element is MaskTextInputFormatter) {
            element.updateMask(
              mask: maskWithPrefix('(##) ####-#####'),
              filter: <String, RegExp>{'#': RegExp(r'[0-9]')},
              newValue: TextEditingValue.empty,
            );
            value = element
                .formatEditUpdate(
                TextEditingValue.empty, TextEditingValue(text: value))
                .text;
          }
          setValue(value);
        }
      }
    }
  }


  void adjustPrefix() {
    if (controller.text.startsWith(widget.prefix)) {
      setControllerCursor();
      return;
    }
    setValue(widget.prefix + controller.text);
  }

  @override
  void onChanged(String? value) {
    if (value != null) {
      verifyPhoneMask(value);
    }
    adjustPrefix();
    super.onChanged(value);
  }

  @override
  void adaptInitialValue({String? value}) {
    value ??= widget.initialValue ?? '';

    String valueWithoutPrefix = value;
    valueWithoutPrefix = valueWithoutPrefix.replaceFirst(widget.prefix, '');
    for (TextInputFormatter element in inputFormatters) {
      if (element is MaskTextInputFormatter) {
        if (valueWithoutPrefix.length > 14) {
          element.updateMask(
            mask: maskWithPrefix('(##) #####-####'),
            filter: <String, RegExp>{'#': RegExp(r'[0-9]')},
            newValue: TextEditingValue.empty,
          );
        } else {
          element.updateMask(
            mask: maskWithPrefix('(##) ####-#####'),
            filter: <String, RegExp>{'#': RegExp(r'[0-9]')},
            newValue: TextEditingValue.empty,
          );
        }
      }
      value = element.formatEditUpdate(
        TextEditingValue.empty,
        TextEditingValue(text: value!),
      ).text;
    }
    controller.text = value!;
    adjustPrefix();
  }
}
