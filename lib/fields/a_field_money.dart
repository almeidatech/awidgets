import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/cupertino.dart';

import 'a_field_text.dart';

class AFieldMoney extends AFieldText {
  final String? leftSymbol;
  final int precision;

  const AFieldMoney({
    super.key,
    super.onChanged,
    super.readOnly,
    super.required,
    super.autofillHints,
    super.icon,
    super.label,
    super.suffix,
    super.bottom,
    this.leftSymbol,
    this.precision = 2,
    super.denySpaces,
    super.hintText,
    required super.identifier,
    super.initialValue = "",
    super.flexible,
    super.expanded,
    super.onUnfocus,
    super.margin,
    super.autofocus,
    super.capitalize,
    super.clearable,
    super.customRules,
    super.height,
    super.linkToAForm,
    super.onSubmit,
    super.padding,
    super.readonly,
    super.timerTime,
  }) : super(
    keyboardType: TextInputType.number,
  );

  @override
  AFieldMoneyState createState() => AFieldMoneyState();
}

class AFieldMoneyState<T extends AFieldMoney> extends AFieldTextState<T> {
  @override
  void initState() {
    super.initState();
    controller = MoneyMaskedTextController(
      decimalSeparator: ',',
      thousandSeparator: '.',
      leftSymbol: widget.leftSymbol == null ? "" : "${widget.leftSymbol!} ",
      initialValue: double.tryParse(widget.initialValue ?? "") ?? 0,
      precision: widget.precision,
    );
  }
}
