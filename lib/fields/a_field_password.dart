import 'package:awidgets/fields/a_field_text.dart';
import 'package:flutter/material.dart';


class AFieldPassword extends AFieldText {
  final void Function(bool)? onSetShowPassword;
  final int minLength;

  const AFieldPassword({
    super.key,
    super.label = "Senha",
    super.identifier = 'password',
    super.readOnly,
    super.onChanged,
    super.customRules,
    this.onSetShowPassword,
    super.flexible,
    super.expanded,
    super.margin,
    this.minLength = 8,
    super.autofocus,
    super.bottom,
    super.clearable,
    super.height,
    super.hintText,
    super.icon,
    super.keyboardType,
    super.linkToAForm,
    super.onSubmit,
    super.onUnfocus,
    super.padding,
    super.readonly,
    super.suffix,
    super.timerTime,
    super.autofillHints = const <String>[AutofillHints.password],
  }) : super(
      required: true,
  );

  @override
  AFieldPasswordState createState() => AFieldPasswordState();
}

class AFieldPasswordState
    <TWidget extends AFieldPassword>
    extends AFieldTextState<TWidget>
{
  bool _showPassword = false;


  String? minPasswordLength(String? value) {
    if (value == null || value.isEmpty) {
      return null;
    }
    if (value.length < widget.minLength) {
      return "A senha deve ter pelo menos ${widget.minLength} caracteres";
    }
    return null;
  }

  void setShowPassword(bool showPassword) {
    if (showPassword == _showPassword) {
      return;
    }
    setState(() {
      _showPassword = showPassword;
    });
    widget.onSetShowPassword?.call(showPassword);
  }

  @override
  void initState() {
    super.initState();
    customRules.add(minPasswordLength);
  }

  @override
  bool get obscure => !_showPassword;
  
  Widget visibilityToggle() => IconButton(
    icon: Icon(
      _showPassword
          ? Icons.visibility_off
          : Icons.visibility,
      color: Colors.grey,
    ),
    onPressed: () => setShowPassword(!_showPassword),
  );
  
  @override
  List<Widget> suffixItems() => <Widget>[
    visibilityToggle(),
    ...super.suffixItems(),
  ];
}
