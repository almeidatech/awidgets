import 'dart:async';
import 'package:cpf_cnpj_validator/cnpj_validator.dart';
import 'package:cpf_cnpj_validator/cpf_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

import 'a_field.dart';

class AFieldCPFCNPJ extends AField<String> {
  final TextInputType? keyboardType;
  final bool clearable;
  final String? hintText;
  final bool autofocus;
  final bool showIcon;
  const AFieldCPFCNPJ({
    this.keyboardType = TextInputType.number,
    this.clearable = false,
    this.showIcon = false,
    this.hintText,
    this.autofocus = false,
    super.key,
    super.readOnly,
    super.identifier = 'document',
    super.required,
    super.label = 'CPF/CNPJ',
    super.onChanged,
    String super.initialValue = '',
    super.flexible,
    super.expanded,
    super.margin,
    super.linkToAForm,
    super.onSubmit,
    super.timerTime,
  });

  @override
  AFieldCPFCNPJState createState() => AFieldCPFCNPJState();
}


String? validate(String? value) {
  if (value == null || value.isEmpty) {
    return null;
  }
  if (value.length < 14) {
    return "Informe um CPF ou CNPJ válido";
  } else if (value.length == 14) {
    if (CPFValidator.isValid(value) == false) {
      return "Informe um CPF válido";
    }
  } else {
    if (CNPJValidator.isValid(value) == false) {
      return "Informe um CNPJ válido";
    }
  }
  return null;
}

class AFieldCPFCNPJState extends AFieldState<AFieldCPFCNPJ, String> {
  Timer? _nextFuture;
  String currentMask = '';
  final TextEditingController controller = TextEditingController();
  final List<TextInputFormatter> inputFormatters = <TextInputFormatter>[
    FilteringTextInputFormatter.deny(RegExp(r'\s')),
    MaskTextInputFormatter(
      mask: '##.###.###/####-##',
      filter: <String, RegExp>{'#': RegExp(r'[0-9]')},
    ),
  ];

  @override
  String get value => controller.text;

  @override
  set value(String? value) => setValue(value ?? "");

  @override
  void clear() => controller.clear();

  void textChanged(String text) {
    if (_nextFuture != null && _nextFuture!.isActive) {
      _nextFuture!.cancel();
    }
    _nextFuture =
        Timer.periodic(const Duration(milliseconds: 500), (Timer timer) {
      widget.onChanged?.call(text);
      timer.cancel();
    });
  }

  @override
  void initState() {
    super.initState();
    defineMask();
    if (inputFormatters.isNotEmpty) {
      adaptInitialValue();
    } else {
      setValue(widget.initialValue ?? '');
    }
  }

  void adaptInitialValue() {
    if (inputFormatters.isNotEmpty) {
      String value = widget.initialValue ?? '';
      value = value.replaceAll(RegExp(r'[^0-9]'), '');
      for (TextInputFormatter element in inputFormatters) {
        if (element is MaskTextInputFormatter) {
          if (value.length > 11) {
            element.updateMask(
              mask: '##.###.###/####-##',
              filter: <String, RegExp>{'#': RegExp(r'[0-9]')},
            );
          } else {
            element.updateMask(
              mask: '###.###.###-###',
              filter: <String, RegExp>{'#': RegExp(r'[0-9]')},
            );
          }
        }
        value = element
            .formatEditUpdate(
                TextEditingValue.empty, TextEditingValue(text: value))
            .text;
      }
      setValue(value);
    }
  }

  @override
  void didUpdateWidget(AFieldCPFCNPJ oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.initialValue != null &&
        widget.initialValue != controller.text) {
      if (inputFormatters.isNotEmpty) {
        adaptInitialValue();
      } else {
        setValue(widget.initialValue ?? '');
      }
    }
  }

  void verifyMask(String value) {
    if (value.length > 14) {
      if (currentMask != '##.###.###/####-##') {
        setState(() {
          currentMask = '##.###.###/####-##';
        });
        for (TextInputFormatter element in inputFormatters) {
          if (element is MaskTextInputFormatter) {
            element.updateMask(
              mask: '##.###.###/####-##',
              filter: <String, RegExp>{'#': RegExp(r'[0-9]')},
            );
            value = element
                .formatEditUpdate(
                    TextEditingValue.empty, TextEditingValue(text: value))
                .text;
          }
        }
        setValue(value);
      }
    } else {
      if (currentMask != '###.###.###-###') {
        setState(() {
          currentMask = '###.###.###-###';
        });
        for (TextInputFormatter element in inputFormatters) {
          if (element is MaskTextInputFormatter) {
            element.updateMask(
              mask: '###.###.###-###',
              filter: <String, RegExp>{'#': RegExp(r'[0-9]')},
              newValue: TextEditingValue.empty,
            );
            value = element
                .formatEditUpdate(
                    TextEditingValue.empty, TextEditingValue(text: value))
                .text;
          }
        }
        setValue(value);
      }
    }
  }

  @override
  Widget childBuild() {
    return Column(children: <Widget>[
      TextFormField(
        autofocus: widget.autofocus == true,
        readOnly: widget.readOnly,
        maxLines: 1,
        autocorrect: false,
        textCapitalization: TextCapitalization.none,
        controller: controller,
        onChanged: (String value) {
          verifyMask(value);
          textChanged(value);
        },
        keyboardType: widget.keyboardType,
        inputFormatters: inputFormatters,
        style: widget.readOnly == true
            ? Theme.of(context).textTheme.bodyLarge!.copyWith(
                  color: Colors.grey[500],
                )
            : null,
        decoration: InputDecoration(
          labelText: widget.label != null && widget.label!.isNotEmpty
              ? '${widget.label} ${widget.required == true ? '*' : ''}'
              : null,
          hintText: widget.hintText,
          filled: widget.readOnly == true,
          suffixIcon: widget.showIcon == false ? null : suffixIcon(),
        ),
        onFieldSubmitted: (_) => widget.onSubmit?.call(),
      ),
      errorText(),
    ]);
  }

  Widget? suffixIcon() {
    if (widget.clearable) {
      return IconButton(
        splashRadius: 20,
        icon: const Icon(Icons.clear, color: Colors.grey),
        onPressed: () {
          controller.clear();
          FocusScope.of(context).unfocus();
        },
      );
    } else {
      IconData? iconData = Icons.contact_emergency;
      return Icon(
        iconData,
        color: Colors.grey,
        size: 20,
      );
    }
  }

  void defineMask() {
    verifyMask(widget.initialValue ?? '');
  }
}
