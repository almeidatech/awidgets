import 'package:awidgets/fields/a_drop_option.dart';
import 'package:awidgets/fields/a_field_drop_down.dart';
import 'package:flutter/material.dart';

class AFieldDropDownLazy<T> extends AFieldDropDown<T> {
  final Future<List<AOption<T>>> Function() loadItems;

  const AFieldDropDownLazy({
    super.key,
    required super.label,
    required super.identifier,
    required this.loadItems,
    super.searchable,
    super.flexible,
    super.expanded,
    super.initialValue,
    super.required,
    super.margin,
    super.onChanged,
    super.height,
    super.width,
    super.linkToAForm,
    super.loading,
    super.menuFooter,
    super.onSubmit,
    super.readOnly,
    super.customRules,
    super.padding,
    super.readonly,
    super.timerTime,
  }) : super(
    options: null,
  );

  @override
  AFieldDropDownLazyState<T> createState() => AFieldDropDownLazyState<T>();
}


class AFieldDropDownLazyState<T> extends AFieldDropDownState<AFieldDropDownLazy<T>, T> {
  @override
  List<AOption<T>> options = <AOption<T>>[];
  @override
  bool loading = true;
  bool error = false;

  @override
  void initState() {
    super.initState();
    loadData();
  }

  void loadData() async {
    try {
      if (!loading || error) {
        setState(() {
          loading = true;
          error = false;
        });
      }
      List<AOption<T>> options = await widget.loadItems();
      setState(() {
        loading = false;
        error = false;
        this.options = options;
        setSelectedOption();
        filterOptions();
      });
    } catch (e, stack) {
      print("$e $stack");
      setState(() {
        loading = false;
        error = true;
      });
    }
  }

  @override
  Widget childBuild() {
    // if (!loading && (error || options.isEmpty)) {
    //   return AUnexpectedError(
    //     noHeight: true,
    //     row: true,
    //     onRefresh: loadData,
    //   );
    // }
    // return ALoadingOverlay(
    //   loading: loading,
    //   content: super.childBuild(),
    // );
    return super.childBuild();
  }
}
