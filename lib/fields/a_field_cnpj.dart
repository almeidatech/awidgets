import 'package:cpf_cnpj_validator/cnpj_validator.dart';
import 'a_field_masked.dart';
import 'package:flutter/material.dart';


class AFieldCNPJ extends AFieldMasked {
  const AFieldCNPJ({
    super.key,
    super.readOnly,
    super.required,
    super.initialValue = "",
    super.onChanged,
    bool? showIcon,
    super.flexible,
    super.expanded,
    super.margin,
    super.label = 'CNPJ',
    super.identifier = 'cnpj',
    super.autofillHints,
    super.autofocus,
    super.bottom,
    super.capitalize,
    super.clearable,
    super.customRules,
    super.filter,
    super.height,
    super.linkToAForm,
    super.onSubmit,
    super.onUnfocus,
    super.padding,
    super.readonly,
    super.suffix,
    super.timerTime,
  }) : super(
          hintText: '00.000.000/0000-00',
          denySpaces: true,
          icon: showIcon == false ? null : Icons.contact_emergency,
          keyboardType: TextInputType.number,
          mask: '##.###.###/####-##',
        );

  @override
  AFieldCNPJState createState() => AFieldCNPJState();
}

String? _validateCNPJ(String? value) {
  if (value == null || value.isEmpty) {
    return null;
  }
  if (CNPJValidator.isValid(value) == false) {
    return "Informe um CNPJ válido";
  }
  return null;
}

class AFieldCNPJState extends AFieldMaskedState {
  @override
  void initState() {
    super.initState();
    customRules.add(_validateCNPJ);
  }
}
