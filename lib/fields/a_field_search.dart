import 'package:awidgets/fields/a_field_text.dart';
import 'package:flutter/material.dart';

class AFieldSearch extends AFieldText {
  const AFieldSearch({
    super.key,
    super.identifier = "search",
    super.label = "Pesquisar",
    super.icon = Icons.search,
    required super.onChanged,
    super.initialValue,
    super.expanded,
    super.flexible,
    super.margin,
    super.height,
    super.autofillHints,
    super.autofocus,
    super.bottom,
    super.capitalize,
    super.customRules,
    super.denySpaces,
    super.hintText,
    super.keyboardType,
    super.linkToAForm = false,
    super.maxLines,
    super.onSubmit,
    super.onUnfocus,
    super.padding,
    super.readOnly,
    super.required,
    super.suffix,
    super.timerTime,
  }) : super(clearable: true);
}
