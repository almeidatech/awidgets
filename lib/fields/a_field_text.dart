import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'a_field.dart';


class AFieldText extends AField<String> {
  final TextInputType? keyboardType;
  final bool clearable;
  final int maxLines;
  final String? hintText;
  final IconData? icon;
  final Iterable<String>? autofillHints;
  final bool capitalize;
  final bool autofocus;
  final bool denySpaces;
  final Widget? suffix;
  final Widget? bottom;
  final void Function()? onUnfocus;
  final double height;
  final InputDecoration? decoration;
  const AFieldText({
    this.capitalize = false,
    this.keyboardType,
    this.clearable = false,
    this.maxLines = 1,
    this.hintText,
    this.autofocus = false,
    this.icon,
    this.suffix,
    this.bottom,
    this.autofillHints,
    this.onUnfocus,
    super.key,
    super.readOnly,
    required super.identifier,
    required super.label,
    super.required,
    super.customRules,
    this.denySpaces = false,
    this.height = 46,
    super.onChanged,
    super.initialValue,
    super.flexible,
    super.expanded,
    super.margin,
    super.linkToAForm,
    super.onSubmit,
    super.padding,
    super.readonly,
    super.timerTime,
    this.decoration,
  });

  @override
  AFieldTextState<AFieldText> createState() => AFieldTextState<AFieldText>();
}


class AFieldTextState<TWidget extends AFieldText> extends AFieldState<TWidget, String> {
  late TextEditingController controller;
  String currentMask = '';
  final List<TextInputFormatter> inputFormatters = <TextInputFormatter>[];
  late FocusNode focusNode;

  @override
  String get value => controller.text;

  @override
  set value(String? value) => controller.text = value ?? "";

  @override
  void clear() {
    controller.clear();
    onChanged(null);
    FocusScope.of(context).unfocus();
  }

  @override
  void initState() {
    super.initState();
    controller = TextEditingController(
      text: widget.initialValue ?? "",
    );
    focusNode = FocusNode(onKeyEvent: __onKeyEvent);
    inputFormatters.addAll(<TextInputFormatter>[
      if (widget.capitalize)
        UpperCaseTextFormatter(),
      if (widget.denySpaces)
        FilteringTextInputFormatter.deny(RegExp(r'\s')),
    ]);
    hintText = widget.hintText;
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  KeyEventResult __onKeyEvent(FocusNode node, KeyEvent event) {
    if (event.logicalKey == LogicalKeyboardKey.escape) {
      clear();
    }
    return KeyEventResult.ignored;
  }

  void setControllerCursor() {
    controller.selection = TextSelection.fromPosition(
      TextPosition(offset: controller.text.length),
    );
  }

  @override
  void didUpdateWidget(TWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    /// do not update based on `initial` value
    // if (
    //   widget.initialValue != null &&
    //   widget.initialValue != controller.text
    // ) {
    //   controller.text = widget.initialValue ?? '';
    //   setControllerCursor();
    // }
  }

  @override
  void setValue(String? value) {
    controller.value = TextEditingValue(
      text: value ?? '',
      selection: controller.value.selection,
      composing: controller.value.composing,
    );
  }

  String? get labelText {
    if (widget.label == null){
      return null;
    }
    String text = widget.label!;
    if (text.isNotEmpty && widget.required) {
      text += " *";
    }
    return text;
  }

  String? hintText;

  Widget textField() => Focus(
    focusNode: focusNode,
    onFocusChange: (bool haveFocus) {
      if (!haveFocus) {
        validate();
        widget.onUnfocus?.call();
      }
    },
    canRequestFocus: false,
    child: TextField(
      autofocus: widget.autofocus,
      readOnly: widget.readOnly,
      obscureText: obscure,
      maxLines: widget.maxLines,
      autocorrect: false,
      autofillHints: widget.autofillHints,
      textCapitalization: TextCapitalization.none,
      controller: controller,
      onChanged: onChanged,
      keyboardType: widget.keyboardType,
      inputFormatters: inputFormatters,
      decoration: widget.decoration ?? InputDecoration(
        border: InputBorder.none,
        contentPadding: EdgeInsets.zero,
        isDense: true,
        labelText: labelText,
        hintText: hintText,
        hintStyle: const TextStyle(
          fontWeight: FontWeight.w400,
        ),
        enabledBorder: InputBorder.none,
        focusedBorder: InputBorder.none,
        errorBorder: InputBorder.none,
        disabledBorder: InputBorder.none,
        isCollapsed: false,
        fillColor: null,
      ),
      onSubmitted: (_) => widget.onSubmit?.call(),
    ),
  );

  List<Widget> suffixItems() => <Widget>[
    if (haveSuffixIcon)
      suffixIcon(),
    if (widget.suffix != null)
      widget.suffix!,
  ];

  @override
  Widget childBuild() {
    return Container(
      constraints: BoxConstraints(minHeight: widget.height),
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(4)),
        color: backgroundColor,
      ),
      padding: widget.padding ??
          const EdgeInsets.symmetric(
            horizontal: 10,
          ),
      clipBehavior: Clip.hardEdge,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Column(
              children: <Widget>[
                const SizedBox(height: 5),
                textField(),
                const SizedBox(height: 2),
                errorText(),
                if (widget.bottom != null)
                  widget.bottom!,
                const SizedBox(height: 5),
              ],
            ),
          ),
          ...suffixItems(),
        ],
      ),
    );
  }

  bool get haveSuffixIcon => widget.clearable || widget.icon != null;

  Widget suffixIcon({final double iconSize = 18}) {
    if (widget.clearable) {
      return AnimatedCrossFade(
        duration: const Duration(milliseconds: 500),
        crossFadeState: value.isEmpty
            ? CrossFadeState.showFirst
            : CrossFadeState.showSecond,
        firstChild: const SizedBox(
          width: 40,
          height: 40,
        ),
        secondChild: IconButton(
          splashRadius: 20,
          icon: Icon(
            Icons.clear,
            color: Colors.grey,
            size: iconSize,
          ),
          onPressed: clear,
        ),
      );
    } else {
      IconData? iconData = widget.icon;
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Icon(
          iconData,
          color: Colors.grey,
          size: iconSize,
        ),
      );
    }
  }
}

class UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
        text: newValue.text.toUpperCase(), selection: newValue.selection);
  }
}
