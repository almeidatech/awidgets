import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

import 'a_field_text.dart';

class AFieldMasked extends AFieldText {
  final String mask;
  final Map<String, RegExp>? filter;
  const AFieldMasked({
    super.key,
    super.onChanged,
    super.readOnly,
    super.required,
    super.autofillHints,
    super.icon,
    super.keyboardType,
    super.label,
    super.suffix,
    super.bottom,
    required this.mask,
    this.filter,
    super.denySpaces,
    required super.hintText,
    required super.identifier,
    super.initialValue = "",
    super.flexible,
    super.expanded,
    super.onUnfocus,
    super.margin,
    super.autofocus,
    super.capitalize,
    super.clearable,
    super.customRules,
    super.height,
    super.linkToAForm,
    super.onSubmit,
    super.padding,
    super.readonly,
    super.timerTime,
  });

  @override
  AFieldMaskedState createState() => AFieldMaskedState();
}

class AFieldMaskedState<T extends AFieldMasked> extends AFieldTextState<T> {
  @override
  void initState() {
    super.initState();
    inputFormatters.add(
      MaskTextInputFormatter(
        mask: widget.mask,
        filter: widget.filter,
        initialText: widget.initialValue,
      ),
    );
  }
}
