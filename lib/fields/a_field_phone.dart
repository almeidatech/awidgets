import 'package:awidgets/fields/a_field_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';


class AFieldPhone extends AFieldText {
  final bool isArgentine;
  const AFieldPhone({
    super.key,
    super.onChanged,
    super.readOnly,
    super.required,
    bool? showIcon,
    super.initialValue = "",
    super.label = "Telefone",
    super.identifier = 'phone',
    this.isArgentine = false,
    super.flexible,
    super.expanded,
    super.margin,
    super.autofocus,
    super.bottom,
    super.capitalize,
    super.clearable,
    super.customRules,
    super.denySpaces,
    super.height,
    super.linkToAForm,
    super.onSubmit,
    super.onUnfocus,
    super.padding,
    super.readonly,
    super.suffix,
    super.timerTime,
    super.hintText ='(00) 0000-0000',
  }) : super(
    icon: showIcon == false ? null : Icons.phone,
    keyboardType: TextInputType.phone,
    autofillHints: const <String>[AutofillHints.telephoneNumber],
  );

  @override
  AFieldPhoneState createState() => AFieldPhoneState();

}


class AFieldPhoneState<T extends AFieldPhone> extends AFieldTextState<T> {


  String? validatePhoneNumber(String? value) {
    if (value == null || value.isEmpty) {
      return null;
    }
    if (!{14, 15}.contains(value.length)) {
      return "Telefone Inválido";
    }
    return null;
  }

  @override
  void initState() {
    super.initState();
    inputFormatters.add(
      MaskTextInputFormatter(
        mask: '(##) #####-####',
        filter: <String, RegExp>{'#': RegExp(r'[0-9]')},
      ),
    );
    verifyPhoneMask(widget.initialValue ?? '');
    customRules.add(validatePhoneNumber);
    adaptInitialValue();
  }

  @override
  void setValue(String? value) {
    super.setValue(value);
    verifyPhoneMask(value ?? '');
  }

  String? phoneValidator() {
    if (value.trim().isEmpty) {
      return null;
    }
    if (widget.isArgentine) {
      if ((value.isNotEmpty) && value.length != 14) {
        return "Utilize um número de celular válido";
      } else {
        return null;
      }
    } else {
      if ((value.isNotEmpty) && (value.length < 14 || value.length > 15)) {
        return "Utilize um número de celular válido";
      } else {
        return null;
      }
    }
  }

  void verifyPhoneMask(String value) {
    if (value.length > 14) {
      if (currentMask != '(##) #####-####') {
        setState(() {
          currentMask = '(##) #####-####';
        });
        for (TextInputFormatter element in inputFormatters) {
          if (element is MaskTextInputFormatter) {
            element.updateMask(
              mask: '(##) #####-####',
              filter: <String, RegExp>{'#': RegExp(r'[0-9]')},
              newValue: TextEditingValue.empty,
            );
            value = element
                .formatEditUpdate(
                TextEditingValue.empty, TextEditingValue(text: value))
                .text;
          }
        }
        setValue(value);
      }
    } else {
      if (currentMask != '(##) ####-#####') {
        setState(() {
          currentMask = '(##) ####-#####';
        });
        for (TextInputFormatter element in inputFormatters) {
          if (element is MaskTextInputFormatter) {
            element.updateMask(
              mask: '(##) ####-#####',
              filter: <String, RegExp>{'#': RegExp(r'[0-9]')},
              newValue: TextEditingValue.empty,
            );
            value = element
                .formatEditUpdate(
                TextEditingValue.empty, TextEditingValue(text: value))
                .text;
          }
          setValue(value);
        }
      }
    }
  }

  @override
  void onChanged(String? value) {
    if (value != null) {
      verifyPhoneMask(value);
    }
    super.onChanged(value);
  }

  void adaptInitialValue({String? value}) {
    value ??= widget.initialValue ?? '';
    for (TextInputFormatter element in inputFormatters) {
      if (element is MaskTextInputFormatter) {
        if (value!.length > 14) {
          element.updateMask(
            mask: '(##) #####-####',
            filter: <String, RegExp>{'#': RegExp(r'[0-9]')},
            newValue: TextEditingValue.empty,
          );
        } else {
          element.updateMask(
            mask: '(##) ####-#####',
            filter: <String, RegExp>{'#': RegExp(r'[0-9]')},
            newValue: TextEditingValue.empty,
          );
        }
      }
      value = element.formatEditUpdate(
        TextEditingValue.empty,
        TextEditingValue(text: value!),
      ).text;
    }
    setValue(value!);
  }
}
