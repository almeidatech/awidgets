// ignore_for_file: must_be_immutable
import 'package:flutter/material.dart';

import 'a_field.dart';
import 'a_option.dart';

class AFieldRadioGroup extends AField<int> {
  final List<Option> options;
  const AFieldRadioGroup({
    required this.options,
    super.onChanged,
    super.key,
    super.readOnly,
    super.required,
    super.initialValue,
    String super.label = '',
    required super.identifier,
    super.margin,
    super.linkToAForm,
    super.onSubmit,
    super.customRules,
    super.expanded,
    super.flexible,
    super.padding,
    super.readonly,
    super.timerTime,
  });

  @override
  State<StatefulWidget> createState() => AFieldRadioState();
}

class AFieldRadioState extends AFieldState<AFieldRadioGroup, int> {
  int? selected;

  @override
  int? get value => selected;

  @override
  set value(int? value) => setState(() {
    selected = value;
  });

  @override
  void initState() {
    super.initState();
    selected = widget.initialValue;
  }

  @override
  void clear() => value = null;

  void onchange(int? value) {
    setState(() {
      selected = value;
    });
    widget.onChanged?.call(value);
  }

  Widget radioOption(String label, int value) {
    return Row(
      children: <Widget>[
        Radio<int>(
          value: value,
          groupValue: selected,
          onChanged: widget.readOnly ? null : onchange
        ),
        if (widget.label != null)
          Expanded(
            child: InkWell(
              onTap: widget.readOnly
                  ? null
                  : () {
                      onchange(value);
                    },
              child: Text(label),
            ),
          ),
      ],
    );
  }

  @override
  Widget childBuild() {
    return Column(children: <Widget>[
      for (final Option option in widget.options)
        radioOption(option.label, option.id),
      errorText(),
    ]);
  }
}
