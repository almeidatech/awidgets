import 'package:awidgets/fields/a_drop_option.dart';
import 'package:awidgets/fields/a_field_drop_down.dart';
import 'package:awidgets/fields/a_field_masked.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'a_field.dart';

class AFieldOAB extends AField<Map<String, dynamic>> {
  final bool autofocus;
  final String oabLabel;
  final String ufLabel;
  const AFieldOAB({
    super.key,
    super.readOnly,
    super.identifier = 'oab',
    this.oabLabel = 'Número OAB',
    this.ufLabel = 'UF',
    super.required,
    super.label,
    super.onChanged,
    this.autofocus = false,
    super.initialValue = const <String, dynamic>{
      'number': '',
      'uf': '',
    },
    super.flexible,
    super.expanded,
    super.margin,
    super.linkToAForm,
    super.onSubmit,
    super.customRules,
    super.padding,
    super.readonly,
    super.timerTime,
  });

  @override
  State<StatefulWidget> createState() => AFieldOABState();
}

class AFieldOABState extends AFieldState<AFieldOAB, Map<String, dynamic>> {
  static const List<String> brUFs = <String>[
    'AC',
    'AL',
    'AP',
    'AM',
    'BA',
    'CE',
    'DF',
    'ES',
    'GO',
    'MA',
    'MT',
    'MS',
    'MG',
    'PA',
    'PB',
    'PR',
    'PE',
    'PI',
    'RJ',
    'RN',
    'RS',
    'RO',
    'RR',
    'SC',
    'SP',
    'SE',
    'TO',
  ];

  // TextEditingController oabNumber = TextEditingController();
  GlobalKey<AFieldMaskedState> oabNumber = GlobalKey<AFieldMaskedState>();
  GlobalKey<AFieldDropDownState<AFieldDropDown<String>, String>> oabUf
      = GlobalKey<AFieldDropDownState<AFieldDropDown<String>, String>>();

  @override
  void initState() {
    super.initState();
  }

  @override
  String? requiredRule<T>(T? value) {
    if (
      value is Map<String, dynamic>
      && (value['number'] == null || value['number'].isEmpty)
      && (value['uf'] == null || value['number'].isEmpty)
    ) {
      return "Campo Obrigatório";
    }
    return null;
  }

  @override
  Map<String, dynamic> get value {
    Map<String, dynamic> val = <String, dynamic>{
      // 'number': oabNumber.value.text,
      'number': oabNumber.currentState?.value,
      'uf': oabUf.currentState?.value,
    };
    print("getting value, returning $val");
    return val;
  }

  @override
  set value(Map<String, dynamic>? value) {
    print("setting value");
    oabNumber.currentState?.value = value?['number'];
    oabUf.currentState?.value = value?['uf'];
  }

  @override
  void clear() {
    print("clearing");
    oabNumber.currentState?.clear();
    oabUf.currentState?.clear();
  }

  @override
  void onChanged(dynamic value) {
    print("called onChanged with value=$value");
    super.onChanged(this.value);
    // validate();
  }

  @override
  Widget childBuild() {
    return Column(children: <Widget>[
      Row(
        children: <Widget>[
          Expanded(
            flex: 2,
            child: AFieldMasked(
              key: oabNumber,
              identifier: '${widget.identifier}.number',
              mask: '###.###.###.###.###.###.###',
              hintText: '0.000',
              label: 'OAB',
              keyboardType: TextInputType.number,
              initialValue: widget.initialValue?['number'],
              onSubmit: widget.onSubmit,
            ),
          ),
          const VerticalDivider(width: 10),
          Expanded(
            child: AFieldDropDown<String>(
              key: oabUf,
              identifier: '${widget.identifier}.uf',
              label: 'UF',
              options: <AOption<String>>[
                for (String uf in brUFs)
                  AOption<String>(label: uf, value: uf),
              ],
              initialValue: widget.initialValue?['uf'],
              searchable: true,
            ),
          ),
        ],
      ),
      errorText(),
    ]);
  }
}
