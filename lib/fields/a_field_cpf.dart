import 'package:cpf_cnpj_validator/cpf_validator.dart';
import 'a_field_masked.dart';
import 'package:flutter/material.dart';


class AFieldCPF extends AFieldMasked {
  const AFieldCPF({
    super.key,
    super.readOnly,
    super.required,
    super.initialValue = '',
    super.onChanged,
    bool? showIcon,
    super.flexible,
    super.expanded,
    super.margin,
    super.label = 'CPF',
    super.identifier = 'cpf',
    super.autofillHints,
    super.autofocus,
    super.bottom,
    super.capitalize,
    super.clearable,
    super.customRules,
    super.filter,
    super.height,
    super.linkToAForm,
    super.onSubmit,
    super.onUnfocus,
    super.padding,
    super.readonly,
    super.suffix,
    super.timerTime,
  }) : super(
          hintText: '000.000.000-00',
          denySpaces: true,
          icon: showIcon == false ? null : Icons.contact_emergency,
          keyboardType: TextInputType.number,
          mask: '###.###.###-##',
        );

  @override
  AFieldCPFState createState() => AFieldCPFState();
}

String? _validateCPF(String? value) {
  if (value == null || value.isEmpty) {
    return null;
  }
  if (CPFValidator.isValid(value) == false) {
    return "Informe um CPF Válido";
  }
  return null;
}

class AFieldCPFState extends AFieldMaskedState {
  @override
  void initState() {
    super.initState();
    customRules.add(_validateCPF);
  }
}
