import 'package:flutter/material.dart';
import 'a_field_text.dart';

class AFieldEmail extends AFieldText {
  const AFieldEmail({
    super.key,
    super.readOnly,
    super.clearable,
    super.required,
    super.initialValue,
    super.onChanged,
    bool showIcon = true,
    super.identifier = 'email',
    super.label = "Email",
    super.flexible,
    super.expanded,
    super.margin,
    super.autofocus,
    super.bottom,
    super.capitalize,
    super.customRules,
    super.height,
    super.hintText,
    super.linkToAForm,
    super.onSubmit,
    super.onUnfocus,
    super.padding,
    super.readonly,
    super.suffix,
    super.timerTime,
  }) : super(
          denySpaces: true,
          icon: showIcon ? Icons.email : null,
          keyboardType: TextInputType.emailAddress,
          autofillHints: const <String>[AutofillHints.email],
        );

  @override
  AFieldEmailState createState() => AFieldEmailState();
}

String? emailValidator(String? value) {
  if (value == null || value.isEmpty) {
    return null;
  }
  const String pattern = r'^[^\s]+@([^\s]+\.)+[^\s]{2,4}$';
  final RegExp regExp = RegExp(pattern);

  if (!regExp.hasMatch(value)) {
    return "Informe um e-mail válido";
  }
  return null;
}

class AFieldEmailState extends AFieldTextState<AFieldEmail> {
  @override
  void initState() {
    super.initState();
    super.customRules.add(emailValidator);
  }
}
