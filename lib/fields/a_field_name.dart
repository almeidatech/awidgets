import 'package:flutter/material.dart';
import 'a_field_text.dart';


class AFieldName extends AFieldText {
  const AFieldName({
    super.key,
    super.readOnly,
    super.clearable,
    super.required,
    super.flexible,
    super.expanded,
    super.margin,
    super.initialValue,
    super.label = 'Nome',
    super.identifier = "name",
    super.autofocus,
    super.bottom,
    super.capitalize,
    super.customRules,
    super.denySpaces,
    super.height,
    super.hintText,
    super.linkToAForm,
    super.maxLines,
    super.onChanged,
    super.onSubmit,
    super.onUnfocus,
    super.padding,
    super.readonly,
    super.suffix,
    super.timerTime,
  }) : super(
          icon: Icons.person,
          keyboardType: TextInputType.name,
          autofillHints: const <String>[AutofillHints.name],
        );
}
