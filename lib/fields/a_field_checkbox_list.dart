import 'a_field_checkbox.dart';
import 'package:flutter/material.dart';

import 'a_field.dart';
import 'a_option.dart';

// ARule<List<int>> _validateMinRequired(int minRequired){
//   return (List<int>? value) {
//     if (
//       minRequired > 0 &&
//       (
//           value == null ||
//           value.length < minRequired
//       )
//     ) {
//       if (minRequired == 1) {
//         return "Selecione ao menos 1 opção";
//       } else {
//         return "Selecione pelo menos $minRequired opções";
//       }
//     }
//     return null;
//   };
// }

class AFieldCheckboxList extends AField<List<int>> {
  final int minRequired;
  final List<Option> options;
  const AFieldCheckboxList({
    super.key,
    required String super.label,
    required super.identifier,
    super.initialValue,
    super.readOnly,
    this.minRequired = 0,
    required this.options,
    super.onChanged,
    super.flexible,
    super.expanded,
    super.margin,
    super.linkToAForm,
    super.onSubmit,
    super.timerTime,
  }) : super(
    required: minRequired > 0,
  );

  @override
  AFieldCheckboxListState createState() => AFieldCheckboxListState();
}

class AFieldCheckboxListState extends AFieldState<AFieldCheckboxList, List<int>> {
  List<int> selectedOptions = <int>[];

  @override
  void initState() {
    super.initState();
    value = widget.initialValue ?? <int>[];
    customRules.add(minRequiredRule);
  }

  String? minRequiredRule(List<int>? value){
    if (
      widget.minRequired > 0 && (
        value == null ||
        value.length < widget.minRequired
      )
    ) {
      if (widget.minRequired == 1) {
        return "Selecione ao menos 1 opção";
      } else {
        return "Selecione pelo menos ${widget.minRequired} opções";
      }
    }
    return null;
  }

  @override
  List<int> get value => selectedOptions;

  @override
  set value(List<int>? value) {
    setState(() {
      selectedOptions.clear();
      if (value != null) {
        selectedOptions.addAll(value);
      }
    });
  }

  @override
  void clear() => setState(() => selectedOptions.clear());

  @override
  Widget childBuild() {
    return Column(
      children: <Widget>[
        Text(
          widget.label ?? '',
          style: const TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(height: 10),
        for (int i = 0; i < widget.options.length; i++) ...<Widget>[
          AFieldCheckbox(
            identifier: '${widget.identifier} $i',
            label: widget.options[i].label,
            initialValue: selectedOptions.contains(widget.options[i].id),
            readOnly: widget.readOnly,
            onChanged: (bool? v) {
              setState(() {
                if (v == true) {
                  selectedOptions.add(widget.options[i].id);
                } else {
                  selectedOptions.remove(widget.options[i].id);
                }
              });
              widget.onChanged?.call(value);
            },
          ),
        ],
        errorText(),
      ],
    );
  }
}
