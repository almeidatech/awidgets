
import 'package:flutter/material.dart';

import 'a_field_password.dart';

class AFieldPasswordNew extends AFieldPassword {
  const AFieldPasswordNew({
    super.key,
    super.label,
    super.identifier,
    super.readOnly,
    super.onChanged,
    super.customRules,
    super.onSetShowPassword,
    super.flexible,
    super.expanded,
    super.margin,
    super.minLength,
    super.autofocus,
    super.bottom,
    super.clearable,
    super.height,
    super.hintText,
    super.icon,
    super.keyboardType,
    super.linkToAForm,
    super.onSubmit,
    super.onUnfocus,
    super.padding,
    super.readonly,
    super.suffix,
    super.timerTime,
    super.autofillHints = const <String>[AutofillHints.newPassword],
  });
}
