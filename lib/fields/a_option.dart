
class Option {
  final String label;
  final int id;
  final List<Option>? children;
  final int? parentId;
  final String? childrenType;
  final bool? initialSelected;
  Option(
    this.label,
    this.id, {
    this.parentId,
    this.childrenType,
    this.children,
    this.initialSelected,
  });
}
