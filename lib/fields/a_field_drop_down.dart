import 'package:awidgets/_internal/a_menu_mixin.dart';
import 'package:awidgets/fields/a_field_search.dart';
import 'package:awidgets/general/a_button.dart';
import 'package:awidgets/utils.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'a_drop_option.dart';
import 'a_field.dart';


class AFieldDropDown<T> extends AField<T> {
  final List<AOption<T>>? options;
  final bool searchable;
  final bool loading;
  final double height;
  final double? width;
  final WidgetBuilder? menuFooter;

  const AFieldDropDown({
    required this.options,
    super.onChanged,
    super.key,
    super.readOnly,
    super.required,
    required super.label,
    required super.identifier,
    this.searchable = false,
    this.loading = false,
    super.initialValue,
    super.flexible,
    super.expanded,
    super.margin,
    this.height = 46,
    this.width,
    this.menuFooter,
    super.linkToAForm,
    super.onSubmit,
    super.customRules,
    super.padding,
    super.readonly,
    super.timerTime,
  });

  @override
  AFieldDropDownState<AFieldDropDown<T>, T> createState() => AFieldDropDownState<AFieldDropDown<T>, T>();
}

class AFieldDropDownState<TWidget extends AFieldDropDown<TValue>, TValue>
    extends AFieldState<TWidget, TValue>
    with AMenuMixin
{

  List<AOption<TValue>>? get options => widget.options!;
  List<AOption<TValue>> filteredOptions = <AOption<TValue>>[];

  String? __searchFilter;

  bool get loading => widget.loading;

  AOption<TValue>? selectedOption;

  TValue? __value;

  @override
  TValue? get value => __value;

  @override
  set value(TValue? value) {
    __value = value;
    setSelectedOption();
  }

  @override
  void clear() => value = null;

  @override
  void initState() {
    super.initState();
    value = widget.initialValue;
    initMenu(
      context,
      menu: menuContent,
      btnMargin: widget.margin,
      menuFooter: menuFooter,
      menuHeader: menuSearchHeader,
    );
    filterOptions();
  }

  @override
  void didUpdateWidget(covariant TWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    filterOptions();
  }

  void setSelectedOption() {
    selectedOption = options?.firstWhereOrNull(
      (AOption<TValue> option) => option.value == value,
    );
    __searchFilter = null;
    filterOptions();
  }

  void filterOptions([BuildContext? context]) {
    if (this.options?.isNotEmpty != true) {
      return;
    }
    List<AOption<TValue>> options = this.options!;
    final String filter = (__searchFilter ?? "").trim();
    if (filter.isEmpty) {
      filteredOptions = List<AOption<TValue>>.from(options);
    } else {
      final List<String> words = (
          removeDiacritics(filter)
              .split(' ')
              .where((String s) => s.isNotEmpty)
              .toList()
      );
      final String anyWord = '(${words.join('|')})';
      RegExp regexFilter = RegExp(
        '.*($anyWord.*){${words.length}}',
        dotAll: true,
        caseSensitive: false,
      );
      filteredOptions = (
          options.where((AOption<TValue> option) {
            return regexFilter.hasMatch(removeDiacritics(option.label));
          })
          .toList()
      );
    }
    if (context != null) {
      (context as Element).markNeedsBuild();
    }
  }

  @override
  void onChanged(TValue? value) {
    setState(() {
      this.value = value;
    });
    super.onChanged.call(value);
  }

  String? get labelText {
    if (widget.label == null){
      return null;
    }
    String text = widget.label!;
    if (text.isNotEmpty && widget.required) {
      text += " *";
    }
    return text;
  }

  Widget menuSearchHeader(BuildContext context) {
    if (!widget.searchable) {
      return const SizedBox();
    }
    return AFieldSearch(
      initialValue: __searchFilter,
      onChanged: (String? value) {
        __searchFilter = value;
        filterOptions(context);
      },
      maxLines: 1,
      margin: const EdgeInsets.all(4),
      autofocus: true,
      onSubmit: () {
        TValue? value = filteredOptions.firstOrNull?.value;
        if (value != null) {
          Navigator.of(context).pop(true);
          onChanged(value);
        }
      },
    );
  }

  Widget menuContent(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        for (AOption<TValue> option in filteredOptions) ...<Widget>[
          AButton(
            text: option.label,
            outlined: true,
            upperCase: false,
            borderColor: Colors.transparent,
            textColor: isDarkMode(context)
                  ? Colors.grey.shade100
                  : Colors.grey.shade800,
            textAlign: TextAlign.start,
            rowAlignment: MainAxisAlignment.start,
            borderRadius: 0,
            textStyle: TextStyle(
              fontWeight: selectedOption?.value == option.value
                  ? FontWeight.w600
                  : null,
            ),
            singleLine: true,
            onPressed: () {
              Navigator.of(context).pop(true);
              onChanged(option.value);
            },
          ),
          // const Divider(height: 0),
        ],
      ],
    );
  }

  @override
  Widget childBuild() {
    return AButton(
      height: widget.height,
      width: widget.width,
      color: backgroundColor,
      elevation: 0,
      padding: const EdgeInsets.symmetric(horizontal: 10),
      onPressed: onPressed,
      disabled: widget.readOnly,
      loading: loading,
      upperCase: false,
      title: labelText == null ? null : Text(
        labelText!,
      ),
      singleLine: true,
      textStyle: const TextStyle(
        fontSize: 15,
      ),
      text: selectedOption?.label ?? "",
      footer: errorText(),
      trailingIcon: Icons.arrow_drop_down,
    );
  }
}
