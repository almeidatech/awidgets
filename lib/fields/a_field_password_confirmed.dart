import 'package:awidgets/fields/a_field.dart';
import 'package:awidgets/fields/a_field_password.dart';
import 'package:flutter/material.dart';


class AFieldPasswordConfirmed extends AFieldPassword {
  const AFieldPasswordConfirmed({
    super.key,
    super.label,
    super.identifier,
    super.flexible,
    super.expanded,
    super.margin,
    super.autofillHints,
    super.autofocus,
    super.bottom,
    super.clearable,
    super.customRules,
    super.height,
    super.hintText,
    super.icon,
    super.keyboardType,
    super.linkToAForm,
    super.minLength,
    super.onChanged,
    super.onSetShowPassword,
    super.onSubmit,
    super.onUnfocus,
    super.padding,
    super.readOnly,
    super.readonly,
    super.suffix,
    super.timerTime,
  });

  @override
  AFieldPasswordConfirmedState createState() => AFieldPasswordConfirmedState();
}

String? confirmPasswordValidator(
    String value, TextEditingController passwordController) {
  if (value.isEmpty) {
    return "Campo Obrigatório";
  }
  if (value != passwordController.text) {
    return "Senha diferente da anterior";
  }
  return null;
}

class AFieldPasswordConfirmedState extends AFieldPasswordState<AFieldPasswordConfirmed> {
  final GlobalKey<AFieldPasswordState> confirmedField = GlobalKey<AFieldPasswordState>();

  AFieldPasswordState? get confirmedState => confirmedField.currentState;

  @override
  void setShowPassword(bool showPassword) {
    super.setShowPassword(showPassword);
    confirmedState?.setShowPassword(showPassword);
  }

  @override
  void onChanged(String? value) {
    if (confirmedState?.value.isNotEmpty == true) {
      confirmedState?.validate();
    }
    super.onChanged(value);
  }

  String? _checkEqualPasswords(String? confirmation) {
    if (confirmation == null || confirmation.isEmpty) {
      return null;
    }
    if (confirmation != value) {
      return "Senha diferente da anterior";
    }
    return null;
  }

  @override
  Widget childBuild() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        super.childBuild(),
        const SizedBox(height: 5),
        AFieldPassword(
          key: confirmedField,
          identifier: '${widget.identifier}_confirmation',
          label: "Confirmação da Senha",
          customRules: <ARule<String>>[
            _checkEqualPasswords,
          ],
          onSetShowPassword: setShowPassword,
          minLength: widget.minLength,
          onSubmit: widget.onSubmit,
          autofillHints: widget.autofillHints,
          padding: widget.padding,
          clearable: widget.clearable,
          height: widget.height,
          linkToAForm: widget.linkToAForm,
        ),
      ],
    );
  }
}
