import 'package:awidgets/general/a_button.dart';
import 'package:awidgets/utils.dart';

import 'a_field.dart';
import 'a_field_masked.dart';
import 'package:flutter/material.dart';

class AFieldDate extends AField<DateTime> {
  const AFieldDate({
    super.key,
    super.readOnly,
    super.required,
    super.label = "Data",
    super.identifier = 'date',
    super.initialValue,
    super.onChanged,
    super.flexible,
    super.expanded,
    super.margin,
    super.linkToAForm,
    super.onSubmit,
    super.timerTime,
  });

  @override
  AFieldDateState createState() => AFieldDateState();
}

class AFieldDateState extends AFieldState<AFieldDate, DateTime> {
  final GlobalKey<AFieldMaskedState> __textField = GlobalKey<AFieldMaskedState>();

  DateTime? __value;
  String? __textValue;

  @override
  DateTime? get value => __value;

  @override
  set value(DateTime? value) {
    __value = value;
    __textValue = formatDate(value);
    __textField.currentState?.value = __textValue;
  }


  @override
  void initState() {
    super.initState();
    value = widget.initialValue;
  }

  @override
  void clear() {
    value = null;
  }

  void onTextChanged(String? text) {
    print("onTextChanged $text");
    __textValue = text;
    try {
      __value = toDateTime(text);
      print("VALID DATE: $value");
      onChanged(value);
    } catch (e) {
      __value = null;
      validate();
    }
  }

  void openCalendarDialog() async {
    print("openCalendarDialog");
  }

  Widget calendarButton() => Padding(
    padding: const EdgeInsets.only(right: 2),
    child: AButton(
      icon: Icons.calendar_month,
      onPressed: openCalendarDialog,
    ),
  );

  @override
  Widget childBuild() {
    return AFieldMasked(
      key: __textField,
      label: widget.label! + (widget.required ? " *":""),
      initialValue: __textValue,
      identifier: '${widget.identifier}_text',
      readOnly: widget.readOnly,
      denySpaces: true,
      keyboardType: TextInputType.number,
      mask: '##/##/####',
      hintText: '00/00/0000',
      onChanged: onTextChanged,
      onUnfocus: validate,
      bottom: errorText(),
      suffix: calendarButton(),
      onSubmit: widget.onSubmit,
      linkToAForm: false,
    );
  }
}
