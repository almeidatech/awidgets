
class AOption<T> {
  final String label;
  final T value;
  const AOption({
    required this.label,
    required this.value,
  });
}
