import 'dart:math';

import 'package:awidgets/general/a_button.dart';
import 'package:awidgets/general/a_paginated.dart';
import 'package:flutter/material.dart';

class TestPagination extends StatefulWidget {
  const TestPagination({super.key});

  @override
  State<StatefulWidget> createState() => _TestPaginationState();

}

class _TestPaginationState extends State<TestPagination> {
  GlobalKey<APaginatedState<int>> list = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              AButton(
                icon: Icons.chevron_left,
                onPressed: () => Navigator.of(context).pop(),
              ),
              const Text("Titulo"),
              AButton(
                text: "Reload",
                onPressed: () => list.currentState?.reload(),
              ),
            ],
          ),
          APaginated<int>(
            key: list,
            expanded: true,
            loadItems: (int limit, int offset) async {
              await Future<void>.delayed(const Duration(seconds: 1));
              const int last = 587;
              limit = min(limit, last - offset);
              return List.generate(limit, (int index) => offset + index);
            },
            itemBuilder: (BuildContext context, int num) => Text("num: $num"),
          ),
        ],
      ),
    );
  }

}
