

import 'dart:math';

import 'package:awidgets/general/a_unexpected_error.dart';
import 'package:awidgets/utils.dart';
import 'package:flutter/material.dart';

class APaginated<T> extends StatefulWidget {
  final Widget Function(BuildContext context, T item) itemBuilder;
  final Future<List<T>> Function(int limit, int offset) loadItems;
  final String emptyMessage;
  final int loaderLimit;
  final bool expanded;
  final bool striped;
  final void Function(T item)? onItemClicked;
  final Color? Function(T)? customRowColor;

  const APaginated({
    super.key,
    required this.itemBuilder,
    required this.loadItems,
    this.loaderLimit = 50,
    this.expanded = false,
    this.striped = true,
    this.onItemClicked,
    this.emptyMessage = "Nenhum item encontrado",
    this.customRowColor,
  });

  @override
  APaginatedState<T> createState() => APaginatedState<T>();

}


class APaginatedState<T> extends State<APaginated<T>> {

  List<T> items = <T>[];
  bool error = false;
  bool loadingFull = true;
  bool loadingMore = false;
  bool hasMore = false;

  ScrollController scrollController = ScrollController();

  int __currentLoad = 0;


  void setLoading(bool loading) {
    setState(() {
      loadingFull = loading;
    });
  }

  void setError(bool error) {
    setState(() {
      this.error = error;
    });
  }


  @override
  void initState() {
    super.initState();
    scrollController.addListener(__onScrolled);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _loadItems();
    });
  }

  void __onScrolled() {
    if (
        scrollController.position.extentAfter < 300
        && !loadingMore
        && hasMore
    ) {
      _loadItems();
    }
  }

  Future<void> reload({bool keepScroll = true}) async {
    __currentLoad++;
    await _loadItems(
      reloadAll: true,
      keepScroll: keepScroll,
    );
  }

  Future<void> _loadItems({
      bool reloadAll = false,
      bool keepScroll = true,
      bool noLimit = false,
  }) async {
    int loadCount = __currentLoad;
    if (reloadAll && keepScroll) {
      scrollController = ScrollController(
        initialScrollOffset: items.isEmpty || error ? 0 : scrollController.offset,
      )..addListener(__onScrolled);
    }
    setState(() {
      if (reloadAll) {
        loadingFull = true;
      }
      error = false;
    });
    int limit = widget.loaderLimit;
    if (noLimit) {
      limit = -1 >>> 1;  // max int value
    }
    int offset = items.length;
    if (reloadAll) {
      if (keepScroll) {
        limit = max(limit, items.length);
      }
      offset = 0;
      items = <T>[];
    }
    try {
      loadingMore = true;
      List<T> moreItems = await widget.loadItems(limit, offset);
      if (loadCount != __currentLoad) {
        return;
      }
      setState(() {
        hasMore = (moreItems.length >= limit);
        items.addAll(moreItems);
        loadingFull = false;
        error = false;
      });
    } catch (e, stack) {
      print("APaginated loadItems error: $e $stack");
      if (loadCount != __currentLoad) {
        return;
      }
      setState(() {
        hasMore = true;
        items = <T>[];
        loadingFull = false;
        error = true;
      });
    } finally {
      if (loadCount == __currentLoad) {
        loadingMore = false;
      }
    }
  }

  Widget _buildItems() => ListView.builder(
    controller: scrollController,
    itemCount: items.length + (hasMore ? 1 : 0),
    itemBuilder: (BuildContext context, int index) {
      if (index == items.length) {
        return _circularProgress();
      }
      T item = items[index];
      Widget itemWidget = widget.itemBuilder(context, item);
      if (
          widget.striped ||
          widget.onItemClicked != null ||
          widget.customRowColor != null
      ) {
        final bool isStriped = (index % 2 == 0);
        final Color stripedColor;
        if (isDarkMode(context)) {
          final Color bg = Theme.of(context).colorScheme.background;
          stripedColor = isStriped ? bg : lighten(bg, 0.10);
        } else {
          stripedColor = isStriped ? Colors.white : Colors.grey.shade200;
        }
        Color? customColor = widget.customRowColor?.call(item);
        return Material(
          color: customColor ?? stripedColor,
          child: InkWell(
            onTap: widget.onItemClicked == null
                ? null
                : () => widget.onItemClicked!.call(item),
            child: itemWidget,
          ),
        );
      }
      return itemWidget;
    },
  );

  Widget _circularProgress() => const Center(
    child: Padding(
      padding: EdgeInsets.all(20),
      child: CircularProgressIndicator(),
    ),
  );

  @override
  Widget build(BuildContext context) {
    Widget content;
    if (loadingFull) {
      content = _circularProgress();
    } else if (error) {
      content = Center(
        child: AUnexpectedError(onRefresh: reload),
      );
    } else if (items.isEmpty) {
      content = Center(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Text(widget.emptyMessage),
        ),
      );
    } else {
      content = _buildItems();
    }
    if (widget.expanded) {
      content = Expanded(child: content);
    }
    return content;
  }

}
