import 'package:flutter/material.dart';

class ALoadingOverlay extends StatelessWidget {
  final bool loading;
  final Widget content;
  const ALoadingOverlay({
    super.key,
    required this.loading,
    required this.content,
  });

  Widget _loadingIndicator(BuildContext context) => Positioned.fill(
    child: Container(
      color: Colors.white.withOpacity(0.4),
      child: Center(
        child: CircularProgressIndicator(
          color: Theme.of(context).primaryColor,
        ),
      ),
    ),
  );

  @override
  Widget build(BuildContext context) => Stack(
    fit: StackFit.loose,
    children: <Widget>[
      content,
      if (loading)
        _loadingIndicator(context),
    ],
  );

}
