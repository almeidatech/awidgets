import 'package:awidgets/general/a_futurebuilder.dart';
import 'package:flutter/material.dart';
import 'a_dialog.dart';

class ADialogLazy<T> extends ADialog {
  final Future<T> Function() future;
  final List<Widget> Function(BuildContext context, T? data, void Function() reload) builder;

  const ADialogLazy({
    super.key,
    required super.title,
    required this.future,
    required this.builder,
    super.titleActions,
    super.persistent,
    super.padding,
  }): super(
    content: const <Widget>[],
  );

  @override
  State<ADialogLazy<T>> createState() => ADialogLazyState<ADialogLazy<T>, T>();

  static Future<bool> show({
    required BuildContext context,
    required String title,
    required List<Widget>? titleActions,
    required List<Widget> content,
    bool persistent = false,
    double padding = 20,
  }) async {
    return true == await showDialog(
      context: context,
      builder: (BuildContext context) => ADialog(
        title: title,
        titleActions: titleActions,
        content: content,
        persistent: persistent,
      ),
    );
  }
}

class ADialogLazyState<TDialog extends ADialogLazy<TValue>, TValue>
    extends ADialogState<TDialog>
{

  Widget contentLazy(BuildContext context, TValue? data, void Function() reload) => Flexible(
    child: SingleChildScrollView(
      padding: EdgeInsets.all(widget.padding).copyWith(top: 0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: widget.builder(context, data, reload),
      ),
    ),
  );

  Widget cardLazy(BuildContext context, TValue? data, void Function() reload) => Container(
    decoration: const BoxDecoration(
      borderRadius: BorderRadius.all(Radius.circular(4)),
    ),
    width: 400,
    child: Column(
      children: <Widget>[
        titleWidget() ?? const SizedBox(),
        contentLazy(context, data, reload),
      ],
    ),
  );

  @override
  Widget card() => AFutureBuilder<TValue>(
    future: widget.future,
    builder: (BuildContext context, TValue? data, void Function() reload) {
      return cardLazy(context, data, reload);
    },
  );

}
