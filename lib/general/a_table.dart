import 'package:awidgets/general/a_paginated.dart';
import 'package:flutter/material.dart';


typedef ACellBuilder<T> = Widget Function(
  BuildContext context,
  double width,
  T item,
);

class ATableColumn<T> {
  final String? title;
  final Widget? titleWidget;
  final ACellBuilder<T> cellBuilder;
  final double? fixedWidth;
  final int expanded;
  final EdgeInsets margin;
  final TextAlign headerAlignment;

  const ATableColumn({
    this.title,
    this.titleWidget,
    required this.cellBuilder,
    this.fixedWidth,
    this.expanded = 1,
    this.margin = const EdgeInsets.symmetric(horizontal: 8),
    this.headerAlignment = TextAlign.left,
  });
}

class ATable<T> extends StatefulWidget {
  final Future<List<T>> Function(int limit, int offset) loadItems;
  final String emptyMessage;
  final int loaderLimit;
  final bool striped;
  final double fontSize;
  final void Function(T item)? onItemClicked;
  final Color? Function(T)? customRowColor;

  final List<ATableColumn<T>> columns;
  final EdgeInsets rowPadding;
  final double mobileBreakpointWidth;
  final double mobileHeaderWidth;

  const ATable({
    super.key,
    required this.columns,
    this.rowPadding = const EdgeInsets.all(8),
    this.striped = true,
    this.loaderLimit = 100,
    this.onItemClicked,
    required this.loadItems,
    this.customRowColor,
    this.fontSize = 14,
    this.mobileBreakpointWidth = 400,
    this.mobileHeaderWidth = 100,
    this.emptyMessage = "Nenhum item encontrado",
  });

  @override
  ATableState<T> createState() => ATableState<T>();

}


class ATableState<T> extends State<ATable<T>> {

  final GlobalKey<APaginatedState<T>> paginated = GlobalKey<APaginatedState<T>>();

  List<T>? get items => paginated.currentState?.items;

  Future<void> reload({bool keepScroll = true}) async {
    await paginated.currentState?.reload(keepScroll: keepScroll);
  }

  void setLoading(bool loading) {
    paginated.currentState?.setLoading(loading);
  }

  void setError(bool error) {
    paginated.currentState?.setError(error);
  }

  Widget cell({
    required ATableColumn<T> column,
    Widget? content,
    bool isVertical = false,
    EdgeInsets extraPadding = EdgeInsets.zero,
  }) {
    Widget widget;
    if (isVertical) {
      widget = Padding(
        padding: column.margin + extraPadding,
        child: content,
      );
    } else {
      widget = Container(
        padding: column.margin,
        width: column.fixedWidth,
        child: content,
      );
      if (column.fixedWidth == null) {
        widget = Expanded(
          flex: column.expanded,
          child: widget,
        );
      }
    }
    return widget;
  }

  Widget headerCell(ATableColumn<T> col, {bool isVertical = false}) => cell(
    column: col,
    isVertical: isVertical,
    content: () {
      if (col.titleWidget != null) {
        return col.titleWidget;
      }
      if (col.title != null) {
        return Text(
          col.title!,
          textAlign: isVertical
              ? TextAlign.start
              : col.headerAlignment,
        );
      }
      return null;
    }(),
  );

  Widget headerContainer({
    required Widget child,
    double borderRadius = 4,
  }) => Container(
    decoration: BoxDecoration(
      // color: isDarkMode(context)
      //     ? Colors.grey.shade800
      //     : Colors.grey.shade300,
      color: Theme.of(context).secondaryHeaderColor,
      borderRadius: BorderRadius.circular(borderRadius),
    ),
    padding: widget.rowPadding,
    child: DefaultTextStyle.merge(
      style: const TextStyle(fontWeight: FontWeight.w500),
      child: child,
    ),
  );

  Widget headerRow() => headerContainer(
    child: Row(
      children: <Widget>[
        for (ATableColumn<T> col in widget.columns)
          headerCell(col, isVertical: false),
      ],
    ),
  );

  Widget row(T item) => Padding(
    padding: widget.rowPadding,
    child: Row(
      children: <Widget>[
        for (ATableColumn<T> col in widget.columns)
          cell(
            column: col,
            content: LayoutBuilder(
              builder: (BuildContext context, BoxConstraints constraints) {
                return col.cellBuilder.call(
                  context,
                  constraints.maxWidth,
                  item,
                );
              },
            ),
          ),
      ],
    ),
  );

  Widget rowVertical(T item) => Padding(
    padding: widget.rowPadding,
    child: LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) => Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          for (int i = 0; i < widget.columns.length; ++i)
            IntrinsicHeight(child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                SizedBox(
                  width: widget.mobileHeaderWidth,
                  child: headerContainer(
                    child: headerCell(
                      widget.columns[i],
                      isVertical: true,
                    ),
                    borderRadius: 0,
                  ),
                ),
                Flexible(
                  child: cell(
                    column: widget.columns[i],
                    isVertical: true,
                    extraPadding: widget.rowPadding,
                    content: widget.columns[i].cellBuilder.call(
                      context,
                      constraints.maxWidth - widget.mobileHeaderWidth,
                      item,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    ),
  );
  
  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final bool useRow = width >= widget.mobileBreakpointWidth;
    return DefaultTextStyle.merge(
      style: TextStyle(fontSize: widget.fontSize),
      child: Column(
        children: <Widget>[
          if (useRow)
            headerRow(),
          APaginated<T>(
            key: paginated,
            expanded: true,
            customRowColor: widget.customRowColor,
            striped: widget.striped,
            itemBuilder: (BuildContext context, T item) {
              return useRow ? row(item) : rowVertical(item);
            },
            loadItems: widget.loadItems,
            onItemClicked: widget.onItemClicked,
          ),
        ],
      ),
    );
  }
}
