import 'package:awidgets/general/a_loading_overlay.dart';
import 'package:expandable_page_view/expandable_page_view.dart';
import 'package:flutter/material.dart';
import '../fields/a_field.dart';
import 'a_button.dart';

/*
Based on:
  https://github.com/flutter/flutter/blob/master/packages/flutter/lib/src/widgets/form.dart
*/


class AForm<T> extends StatefulWidget {
  final List<Widget> fields;
  final List<Widget>? rightFields;
  final Widget? fieldsTitle;
  final Widget? rightFieldsTitle;
  final List<Widget> actions;
  final dynamic initialData;
  final Color? backgroundColor;
  final String? submitText;
  final T Function(dynamic) fromJson;
  final Future<void> Function()? onCancelled;
  final Future<String?> Function(T) onSubmit;
  final void Function()? onSuccess;
  final bool showAllForm;
  final bool showDefaultAction;
  final Color? actionColor;
  final bool backIcon;
  final bool backButton;
  final Function(T)? setterChanges;
  final double minDualColumWidth;
  final EdgeInsets padding;
  final double fieldsPadding;
  final double actionsPadding;
  final void Function(bool loading)? onLoadingChange;
  final Widget? topBar;
  final bool obscureAll;

  const AForm({
    super.key,
    required this.fields,
    this.rightFields,
    required this.fromJson,
    this.actions = const <Widget>[],
    this.backgroundColor,
    this.backIcon = true,
    this.backButton = false,
    this.obscureAll = false,
    this.initialData,
    this.submitText,
    this.padding = const EdgeInsets.all(20),
    this.fieldsPadding = 5,
    this.actionsPadding = 5,
    required this.onSubmit,
    this.onCancelled,
    this.onSuccess,
    this.onLoadingChange,
    this.showAllForm = true,
    this.showDefaultAction = true,
    this.actionColor,
    this.setterChanges,
    this.minDualColumWidth = 600,
    this.fieldsTitle,
    this.rightFieldsTitle,
    this.topBar,
  });

  @override
  State<AForm<T>> createState() => AFormState<T>();

  static AFormState? maybeOf(BuildContext context) {
    final _AFormScope? scope = context.dependOnInheritedWidgetOfExactType<_AFormScope>();
    return scope?.formState;
  }
}

enum KeyType {list, map}

abstract class __Key<T> {
  T get key;
}

class __ListKey extends __Key<int> {
  @override
  final int key;
  __ListKey(this.key);
}

class __MapKey extends __Key<String> {
  @override
  final String key;
  __MapKey(this.key);
}

List<__Key<dynamic>> _extractKeys(String identifier) {
  List<__Key<dynamic>> keys = <__Key<dynamic>>[];
  for (String key in identifier.split('.')) {
    int? index = int.tryParse(key);
    if (index != null) {
      keys.add(__ListKey(index));
    } else {
      keys.add(__MapKey(key));
    }
  }
  return keys;
}

dynamic __initializedValue({
  __Key<dynamic>? key,
  String? identifier,
}) {
  assert (key != null || identifier != null);
  key ??= _extractKeys(identifier!).first;
  if (key is __ListKey) {
    return List<dynamic>.filled(key.key+1, null, growable: true);
  } else if (key is __MapKey) {
    return <String, dynamic>{};
  }
  throw Exception("Unexpected key type");
}

dynamic __getMapNav(dynamic json, List<__Key<dynamic>> keys) {
  dynamic nav = json;
  for (int i = 0; i < keys.length-1; ++i) {
    __Key<dynamic> current = keys[i];
    __Key<dynamic> next = keys[i+1];
    if (nav is List && nav.length < current.key+1) {
      for (int i = nav.length; i < current.key+1; ++i) {
        nav.add(
          __initializedValue(key: next),
        );
      }
    }
    if (nav[current.key] == null) {
      nav[current.key] = __initializedValue(key: next);
    }
    nav = nav[current.key];
  }
  return nav;
}

dynamic _getValue(dynamic json, String identifier) {
  if (json == null) {
    return null;
  }
  List<__Key<dynamic>> keys = _extractKeys(identifier);
  if (json is Map) {
    json = Map.from(json);
  } else if (json is List) {
    json = List.from(json);
  }
  dynamic nav = __getMapNav(json, keys);
  return nav[keys.last.key];
}

void _setMapValue(dynamic json, String identifier, dynamic value) {
  List<__Key<dynamic>> keys = _extractKeys(identifier);
  if (json == null) {
    return;
  }
  dynamic nav = __getMapNav(json, keys);
  nav[keys.last.key] = value;
}

class AFormState<T> extends State<AForm<T>> {
  bool loading = false;
  bool showAllForm = false;
  int page = 0;

  bool fieldValidated = false;
  PageController pageController = PageController();
  final ScrollController scrollController =  ScrollController();

  final Map<String, AFieldState> fieldStates = <String, AFieldState>{};
  int _generation = 0;

  void addField(AFieldState fieldState) {
    fieldStates[fieldState.widget.identifier] = fieldState;
    dynamic initialValue = _getValue(
        widget.initialData,
        fieldState.widget.identifier,
    );
    if (initialValue != null) {
      fieldState.setState(() {
        fieldState.value = initialValue;
      });
    }
    if (widget.obscureAll) {
      fieldState.setState(() {
        fieldState.obscure = true;
      });
    }
  }

  void removeField(AFieldState fieldState) {
    fieldStates.remove(fieldState.widget.identifier);
  }

  @override
  void initState() {
    super.initState();
    setup();
  }

  @override
  void dispose() {
    for (AFieldState state in fieldStates.values) {
      state.dispose();
    }
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant AForm<T> oldWidget) {
    super.didUpdateWidget(oldWidget);
    setup();
    if (showAllForm == false) {
      if (oldWidget.fields.length != widget.fields.length) {
        if (oldWidget.fields.length > widget.fields.length) {
          pageController.animateToPage(
            widget.fields.length - 1,
            duration: const Duration(milliseconds: 300),
            curve: Curves.ease,
          );
        }
      }
    }
  }

  void clear() {
    for (AFieldState entry in fieldStates.values) {
      entry.clear();
    }
  }

  void setup() {
    if (widget.fields.length == 1 || widget.showAllForm) {
      showAllForm = true;
    }
  }

  T getData() {
    dynamic json = widget.initialData;
    for (MapEntry<String, AFieldState> entry in fieldStates.entries) {
      json ??= __initializedValue(identifier: entry.key);
      _setMapValue(json, entry.key, entry.value.value);
    }
    return widget.fromJson(json);
  }

  Widget _buildFields(List<Widget> fields, Widget? title) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        if (title != null) ...<Widget>[
          const SizedBox(height: 10),
          title,
          SizedBox(height: widget.fieldsPadding),
        ],
        ...spacedWidgets(fields, margin: widget.fieldsPadding),
      ],
    );
  }

  void onBack() {
    pageController.animateToPage(
      pageController.page!.toInt() - 1,
      duration: const Duration(milliseconds: 300),
      curve: Curves.easeIn,
    );
  }

  void onSubmit() async {
    if (validate()) {
      setState(() {
        loading = true;
      });
      widget.onLoadingChange?.call(true);
      String? errorMessage;
      try {
        errorMessage = await widget.onSubmit(getData());
      } catch (e, stack) {
        errorMessage = "Erro Inesperado";
        print("$e $stack");
      }
      setState(() {
        loading = false;
      });
      widget.onLoadingChange?.call(false);
      if (errorMessage != null) {
        showErrorMessage(errorMessage);
      } else {
        widget.onSuccess?.call();
      }
    }
  }

  void showErrorMessage(String text) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(
        text,
        style: const TextStyle(color: Colors.white),
      ),
      backgroundColor: Colors.red,
    ));
  }

  bool validate() {
    bool isOk = true;
    for (AFieldState fieldState in fieldStates.values) {
      if (!fieldState.validate()) {
        isOk = false;
      }
    }
    return isOk;
  }

  Widget _pageView() {
    return IntrinsicHeight(
      child: ExpandablePageView.builder(
        physics: const NeverScrollableScrollPhysics(),
        onPageChanged: (int page) {
          setState(() {
            this.page = page;
            fieldValidated = false;
            FocusManager.instance.primaryFocus?.unfocus();
          });
          if (widget.setterChanges != null) {
            widget.setterChanges?.call(getData());
          }
        },
        controller: pageController,
        itemCount: widget.fields.length,
        itemBuilder: (BuildContext context, int index) {
          return widget.fields.isNotEmpty
              ? Center(
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: widget.padding,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.all(widget.fieldsPadding),
                            child: widget.fields[index],
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              : const Center(
                  child: Text('Nenhum campo foi adicionado'),
                );
        },
      ),
    );
  }

  Widget containerAppBar() {
    if (showAllForm == false && page != 0 && widget.backIcon == true) {
      return Container(
        color: Colors.transparent,
        child: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          automaticallyImplyLeading: true,
          actions: <Widget>[Container()],
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_rounded, color: Colors.black),
            onPressed: loading ? null : onBack,
          ),
        ),
      );
    } else {
      return Container(
        color: Colors.transparent,
        child: widget.fields.length > 1
            ? AppBar(
                //Para não sambar o appbar
                backgroundColor: Colors.transparent,
                elevation: 0,
                automaticallyImplyLeading: true,
                actions: <Widget>[Container()],
                leading: const SizedBox(),
                toolbarHeight: 0,
              )
            : null,
      );
    }
  }

  Widget saveAction() => AButton(
    text: widget.submitText ?? "Salvar",
    onPressed: onSubmit,
    color: widget.actionColor ?? Theme.of(context).primaryColor,
    textColor: Colors.white,
    borderColor: widget.actionColor,
  );

  Widget nextAction() => AButton(
    // loading: loading,
    color: Theme.of(context).primaryColor,
    textColor: Colors.white,
    text: "Próximo",
    onPressed: () async {
      validate();
      if (fieldValidated) {
        await pageController.animateToPage(
          pageController.page!.toInt() + 1,
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeIn,
        );
      }
    },
  );

  List<Widget> spacedWidgets(
      List<Widget> widgets,
      {
        double? margin,
      }
  ) => <Widget>[
    for (Widget wid in widgets) ...<Widget>[
      if (wid != widgets.first)
        SizedBox(
          width: margin ?? widget.padding.vertical,
          height: margin ?? widget.padding.vertical,
        ),
      wid,
    ],
  ];

  bool get isLastPage => (page == widget.fields.length - 1);

  List<Widget> defaultActions(Color primaryColor) => <Widget>[
    if (widget.onCancelled != null)
      AButton(
        color: Colors.transparent,
        textColor: primaryColor,
        elevation: 0,
        text: "CANCELAR",
        onPressed: widget.onCancelled!,
      ),
    if (!widget.showAllForm) ...<Widget>[
      if (widget.backButton && page > 0)
        AButton(
          color: Colors.transparent,
          elevation: 0,
          borderColor: primaryColor,
          textColor: primaryColor,
          text: "Voltar",
          onPressed: onBack,
        ),
    ],
    if (widget.showAllForm || isLastPage)
      saveAction()
    else if (!widget.showAllForm && !isLastPage)
      nextAction(),
  ];

  Widget bottomNavigationContainer() {
    Color primaryColor = Theme.of(context).primaryColor;
    List<Widget> beforeSpacer = <Widget>[];
    List<Widget> afterSpacer = <Widget>[];
    int spacerIndex = widget.actions.indexWhere((Widget w) => w is Spacer);
    if (spacerIndex >= 0) {
      beforeSpacer = widget.actions.sublist(0, spacerIndex);
      afterSpacer = widget.actions.sublist(spacerIndex+1);
    } else {
      afterSpacer = widget.actions;
    }
    return Row(
      children: <Widget>[
        ...spacedWidgets(beforeSpacer, margin: widget.actionsPadding),
        SizedBox(width: widget.actionsPadding),
        Expanded(
          child: Wrap(
            runSpacing: widget.actionsPadding,
            spacing: widget.actionsPadding,
            direction: Axis.horizontal,
            alignment: WrapAlignment.end,
            runAlignment: WrapAlignment.end,
            crossAxisAlignment: WrapCrossAlignment.center,
            children: <Widget>[
              ...afterSpacer,
              if (widget.showDefaultAction)
                ...defaultActions(primaryColor),
              // ...spacedWidgets(afterSpacer, margin: widget.actionsPadding),
              // if (widget.showDefaultAction) ...<Widget>[
              //   // const Spacer(),
              //   SizedBox(width: widget.actionsPadding),
              //   ...spacedWidgets(defaultActions(primaryColor), margin: widget.actionsPadding),
              // ],
            ],
          ),
        ),
      ],
    );
  }

  bool get useDoubleColumn => (
      widget.rightFields != null &&
      MediaQuery.of(context).size.width >= widget.minDualColumWidth
  );

  Widget form() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        if (widget.topBar != null)
          Padding(
            padding: widget.padding.copyWith(top: 0, bottom: 0),
            child: widget.topBar!,
          ),
        containerAppBar(),
        Flexible(
          child: SingleChildScrollView(
            controller: scrollController,
            padding: widget.padding,
            child: Form(
              // key: _formKey,
              autovalidateMode: AutovalidateMode.disabled,
              child: (() {
                if (!showAllForm) {
                  return _pageView();
                } else if (useDoubleColumn) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        child: _buildFields(widget.fields, widget.fieldsTitle),
                      ),
                      SizedBox(width: widget.fieldsPadding),
                      Expanded(
                        child: _buildFields(widget.rightFields!, widget.rightFieldsTitle),
                      ),
                    ],
                  );
                } else {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      _buildFields(widget.fields, widget.fieldsTitle),
                      if (widget.rightFields != null)
                        _buildFields(
                          widget.rightFields!,
                          widget.rightFieldsTitle,
                        ),
                    ],
                  );
                }
              })(),
            ),
          ),
        ),
        SizedBox(height: widget.fieldsPadding),
        Padding(
          padding: widget.padding.copyWith(top: 0, bottom: 0),
          child: bottomNavigationContainer(),
        ),
      ],
    );
  }

  // void _forceRebuild() {
  //   setState(() {
  //     ++_generation;
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return _AFormScope(
      formState: this,
      generation: _generation,
      child: Container(
        color: widget.backgroundColor,
        child: ALoadingOverlay(
          loading: loading,
          content: Padding(
            padding: widget.padding.copyWith(left: 0, right: 0),
            child: form(),
          ),
        ),
      ),
    );
  }
}


class _AFormScope extends InheritedWidget {
  final AFormState formState;
  final int _generation;

  const _AFormScope({
    required super.child,
    required this.formState,
    required int generation,
  }) : _generation = generation;

  @override
  bool updateShouldNotify(covariant _AFormScope old) {
    return _generation != old._generation;
  }

}
