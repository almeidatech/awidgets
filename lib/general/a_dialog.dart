import 'package:awidgets/utils.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'a_button.dart';

@Deprecated("Use ADialogV2 instead")
class ADialog extends StatefulWidget {
  final String? title;
  final List<Widget>? titleActions;
  final List<Widget>? content;
  final bool persistent;
  final double padding;
  final double width;
  final double? height;
  final bool useInternalScroll;

  const ADialog({
    super.key,
    this.title,
    this.content,
    this.titleActions,
    this.persistent = false,
    this.useInternalScroll = false,
    this.padding = 20,
    this.width = 400,
    this.height,
  });

  @override
  State<ADialog> createState() => ADialogState<ADialog>();

  static Future<bool> show({
    required BuildContext context,
    String? title,
    required List<Widget> content,
    bool useInternalScroll = false,
    bool persistent = false,
    double padding = 20,
    double width = 400,
    double? height,
    List<Widget>? titleActions,
  }) async {
    return true == await showDialog(
      context: context,
      barrierColor: Colors.transparent,
      builder: (BuildContext context) => ADialog(
        title: title,
        titleActions: titleActions,
        content: content,
        persistent: persistent,
        padding: padding,
        width: width,
        useInternalScroll: useInternalScroll,
        height: height,
      ),
    );
  }
}

class ADialogState<T extends ADialog> extends State<T> {
  List<Widget>? contentt;

  @override
  void initState() {
    super.initState();
    contentt = widget.content != null ? List<Widget>.from(widget.content!) : null;
  }

  void close(BuildContext context) {
    if (!widget.persistent) {
      Navigator.of(context).pop(false);
    }
  }

  Widget titleText() => Text(
    widget.title ?? '',
    style: const TextStyle(
      fontSize: 18,
      fontWeight: FontWeight.w500,
    ),
    overflow: TextOverflow.clip,
  );

  Widget titleActions() {
    if (widget.titleActions == null) {
      return SizedBox(width: widget.padding);
    }
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: widget.padding),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: widget.titleActions!,
      ),
    );
  }

  Widget? titleWidget() => Padding(
    padding: EdgeInsets.all(widget.padding),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Expanded(child: titleText()),
        titleActions(),
        if (!widget.persistent)
          AButton(
            onPressed: () {
              close(context);
            },
            icon: Icons.close,
          ),
      ],
    ),
  );

  Widget content() => Flexible(
    child: SingleChildScrollView(
      padding: EdgeInsets.all(widget.padding).copyWith(top: 0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: contentt!,
      ),
    ),
  );

  Widget card() => AnimatedSize(
    duration: const Duration(milliseconds: 400),
    child: Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(4)),
      ),
      width: widget.width,
      height: widget.height,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          titleWidget() ?? const SizedBox(),
          if (widget.useInternalScroll)
            content()
          else
            ...contentt!,
        ],
      ),
    ),
  );

  Widget container() => Center(
    child: Container(
      // avoids closing on clicks near the card
      padding: const EdgeInsets.all(20),
      color: Colors.transparent,
      child: Card(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(4)),
        ),
        color: Theme.of(context).colorScheme.background,
        child: card(),
      ),
    ),
  );

  void rebuildWithNewContent(List<Widget>? newContent) {
    setState(() {
      contentt = newContent;
    });
    if (kDebugMode){
      print("rebuildWithNewContent");
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapUp: (_) => close(context),
      child: Scaffold(
        // backgroundColor: (
        //     isDarkMode(context)
        //         ? Colors.black.withOpacity(0.01)
        //         : Colors.white.withOpacity(0.1)
        // ),
        backgroundColor: (
            isDarkMode(context)
                ? Colors.black.withOpacity(0.01)
                : Colors.black.withOpacity(0.2)
        ),
        body: GestureDetector(
          onTapUp: (_) {/* ignore taps on card */},
          child: container(),
        ),
      ),
    );
  }
}

class ADialogV2<T> extends ADialog {
  const ADialogV2({
    super.key,
    super.title,
    super.content,
    super.titleActions,
    super.persistent,
    super.useInternalScroll,
    super.padding,
    super.width,
    super.height,
  });

  static Future<T?> show<T>({
    required BuildContext context,
    String? title,
    required List<Widget> content,
    bool useInternalScroll = false,
    bool persistent = false,
    double padding = 20,
    double width = 400,
    double? height,
    List<Widget>? titleActions,
    Key? key,
  }) async {
    return await showDialog<T>(
      context: context,
      barrierColor: Colors.transparent,
      builder: (BuildContext context) => ADialogV2<T>(
        key: key,
        title: title,
        titleActions: titleActions,
        content: content,
        persistent: persistent,
        padding: padding,
        width: width,
        useInternalScroll: useInternalScroll,
        height: height,
      ),
    );
  }

  @override
  State<ADialogV2<T>> createState() => ADialogV2State<T>();
}

class ADialogV2State<T> extends ADialogState<ADialogV2<T>> {
  @override
  void close(BuildContext context, [T? result]) {
    if (!widget.persistent) {
      Navigator.of(context).pop(result);
    }
  }

}
