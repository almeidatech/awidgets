import 'package:awidgets/general/a_button.dart';
import 'package:flutter/material.dart';

class AUnexpectedError extends StatelessWidget {
  final Function() onRefresh;
  final String? message;
  final String? buttonMessage;
  final MainAxisAlignment mainAxisAlignment;
  final CrossAxisAlignment crossAxisAlignment;
  final double? customHeight;
  final bool noHeight;
  final bool row;
  const AUnexpectedError({
    Key? key,
    required this.onRefresh,
    this.message,
    this.buttonMessage,
    this.mainAxisAlignment = MainAxisAlignment.center,
    this.crossAxisAlignment = CrossAxisAlignment.center,
    this.customHeight,
    this.noHeight = false,
    this.row = false,
  }) : super(key: key);

  SizedBox get spacing => const SizedBox(width: 10, height: 10);

  List<Widget> content() => <Widget>[
    spacing,
    Text(
      message == null ? "Ocorreu um erro inesperado" : message!,
      style: const TextStyle(fontWeight: FontWeight.bold),
      textAlign: TextAlign.center,
    ),
    spacing,
    AButton(
      onPressed: onRefresh,
      elevation: 0,
      padding: const EdgeInsets.all(8),
      text: buttonMessage ?? "Tentar Novamente",
    ),
    spacing,
  ];

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;

    return SizedBox(
      height: noHeight ? null : customHeight ?? height,
      child: row
        ? Row(
          mainAxisAlignment: mainAxisAlignment,
          crossAxisAlignment: crossAxisAlignment,
          children: content(),
        ) : Column(
          mainAxisAlignment: mainAxisAlignment,
          crossAxisAlignment: crossAxisAlignment,
          children: content(),
        ),
    );
  }
}
