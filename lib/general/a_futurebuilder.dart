import 'package:awidgets/general/a_loading_overlay.dart';
import 'package:awidgets/general/a_unexpected_error.dart';
import 'package:flutter/material.dart';


class AFutureBuilder<T> extends StatefulWidget {
  final Future<T> Function() future;
  final Widget Function(BuildContext context, T? data, void Function() reload) builder;
  final bool insideRow;
  
  const AFutureBuilder({
    super.key,
    required this.future,
    required this.builder,
    this.insideRow = true,
  });

  @override
  AFutureBuilderState<T> createState() => AFutureBuilderState<T>();
}


class AFutureBuilderState<T> extends State<AFutureBuilder<T>> {
  T? data;
  bool error = false;
  bool loading = true;

  @override
  void initState() {
    super.initState();
    loadData();
  }

  void loadData() async {
    try {
      if (error || !loading) {
        setState(() {
          error = false;
          loading = true;
        });
      }
      T data = await widget.future.call();
      setState(() {
        loading = false;
        this.data = data;
      });
    } catch (e, stack) {
      print("$e $stack");
      setState(() {
        error = true;
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (error) {
      return AUnexpectedError(
        onRefresh: loadData,
        row: widget.insideRow,
      );
    }
    return ALoadingOverlay(
      loading: loading,
      content: widget.builder.call(
        context,
        data,
        loadData,
      ),
    );
  }
}
