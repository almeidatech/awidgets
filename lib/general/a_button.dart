import 'package:awidgets/utils.dart';
import 'package:flutter/material.dart';


class AButton extends StatefulWidget {
  final String? text;
  final String? textTitle;
  final Widget? title;
  final TextStyle? textStyle;
  final TextAlign textAlign;
  final IconData? icon;
  final Function()? onPressed;
  final bool loading;
  final bool disabled;
  final bool upperCase;
  final Color? color;
  final EdgeInsets padding;
  final EdgeInsets margin;
  final Color? borderColor;
  final double? borderRadius;
  final bool outlined;
  final double? height;
  final double? width;
  final double elevation;
  final double fontSize;
  final FontWeight? fontWeight;
  final IconData? landingIcon;
  final Color? landingColor;
  final Widget? landingImage;
  final IconData? trailingIcon;
  final Color? trailingColor;
  final Color? textColor;
  final MainAxisAlignment rowAlignment;
  final bool singleLine;
  final double iconSize;
  final bool expanded;
  final Widget? suffixChip;
  final Widget? footer;

  const AButton({
    super.key,
    this.text,
    this.textTitle,
    this.textStyle,
    this.textAlign = TextAlign.center,
    this.icon,
    required this.onPressed,
    this.disabled = false,
    this.loading = false,
    this.color,
    this.upperCase = true,
    this.borderColor,
    this.borderRadius,
    this.fontSize = 12,
    this.iconSize = 18,
    this.fontWeight,
    this.height,
    this.width,
    this.elevation = 2,
    this.landingIcon,
    this.landingColor,
    this.landingImage,
    this.trailingIcon,
    this.trailingColor,
    this.padding = const EdgeInsets.all(10),
    this.margin = EdgeInsets.zero,
    this.textColor,
    this.outlined = false,
    this.rowAlignment = MainAxisAlignment.center,
    this.singleLine = false,
    this.expanded = false,
    this.suffixChip,
    this.title,
    this.footer,
  }) : assert(height == null || height >= 30),
        assert(text != null || icon != null),
        assert(text == null || icon == null);

  @override
  AButtonState<AButton> createState() => AButtonState<AButton>();
}

class AButtonState<P extends AButton> extends State<P> {

  Color get backgroundColor {
    if (widget.outlined) {
      return Colors.transparent;
    } else if (widget.color != null) {
      return widget.color!;
    } else if (isIcon) {
      return Colors.grey.shade200;
    } else {
      return Theme.of(context).primaryColor;
    }
  }

  Color get foregroundColor {
    if (widget.textColor != null) {
      return widget.textColor!;
    }
    if (widget.outlined) {
      return borderColor;
    }
    return foregroundColorByLuminance(backgroundColor);
  }

  Color get borderColor {
    if (haveBorder) {
      return (
        widget.borderColor ??
        widget.color ??
        Theme.of(context).colorScheme.primary
      );
    }
    return Colors.transparent;
  }

  double get elevation {
    if (widget.outlined || isIcon) {
      return 0;
    }
    return widget.elevation;
  }

  bool get isActive => (!widget.disabled && !widget.loading);
  bool get haveBorder => (widget.outlined || widget.borderColor != null);

  bool get isIcon => widget.icon != null;

  BorderRadius get borderRadius {
    if (widget.borderRadius != null) {
      return BorderRadius.circular(widget.borderRadius!);
    }
    return BorderRadius.circular(isIcon ? 100 : 4);
  }

  void onPressed() => widget.onPressed?.call();

  Widget content() => Row(
    crossAxisAlignment: CrossAxisAlignment.center,
    mainAxisAlignment: widget.rowAlignment,
    mainAxisSize: MainAxisSize.min,
    children: <Widget>[
      if (widget.landingIcon != null) ...<Widget>[
        Icon(
          widget.landingIcon!,
          color: widget.landingColor ?? foregroundColor,
          size: widget.iconSize,
        ),
        const SizedBox(width: 5),
      ],
      if (widget.landingImage != null) ...<Widget>[
        widget.landingImage!,
        const SizedBox(width: 5),
      ],
      if (widget.text != null) Flexible(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            if (widget.title != null)
              widget.title!,
            if (widget.textTitle != null)
              Text(
                widget.textTitle!,
                style: TextStyle(
                  fontSize: widget.fontSize * .8,
                  fontWeight: FontWeight.w300,
                ),
                overflow: widget.singleLine
                    ? TextOverflow.ellipsis
                    : TextOverflow.clip,
                textAlign: TextAlign.left,
              ),
            Text(
              widget.upperCase
                  ? widget.text!.toUpperCase()
                  : widget.text!,
              style: widget.textStyle ?? TextStyle(
                fontSize: widget.fontSize,
                fontWeight: widget.fontWeight ?? FontWeight.w400,
              ),
              overflow: widget.singleLine
                  ? TextOverflow.ellipsis
                  : TextOverflow.clip,
              textAlign: widget.textAlign,
            ),
            if (widget.footer != null)
              widget.footer!,
          ],
        ),
      ),
      if (widget.icon != null)
        Icon(
          widget.icon,
          size: widget.iconSize,
          color: foregroundColor,
        ),
      if (widget.trailingIcon != null) ...<Widget>[
        const SizedBox(width: 5),
        Icon(
          widget.trailingIcon!,
          color: widget.trailingColor ?? foregroundColor,
          size: widget.iconSize,
        ),
      ],
      if (widget.suffixChip != null) ...<Widget>[
        Container(
          height: widget.height == null ? null : double.infinity,
          // padding: widget.padding / 4,
          // padding: EdgeInsets.symmetric(
          //   vertical: widget.padding.vertical / 4,
          //   horizontal: widget.padding.horizontal / 2,
          // ),
          constraints: BoxConstraints(
            minWidth: widget.height ?? 0.0,
          ),
          padding: EdgeInsets.symmetric(
            vertical: widget.padding.vertical / 4,
            horizontal: (widget.padding.horizontal / 4) * 1.5,
          ),
          margin: (widget.padding / 2).copyWith(left: widget.padding.left / 2),
          decoration: BoxDecoration(
            // color: Colors.grey.withOpacity(0.25),
            color: darken(backgroundColor, 0.1),
            borderRadius: borderRadius,
          ),
          child: DefaultTextStyle.merge(
            style: TextStyle(
              color: foregroundColor,
              fontSize: widget.fontSize,
            ),
            child: Center(
              child: widget.suffixChip!,
            ),
          ),
        ),
      ],
    ],
  );

  Widget loadingIndicator() => Positioned.fill(
    child: Container(
      decoration: BoxDecoration(
        borderRadius: borderRadius,
        color: Colors.grey.withAlpha(150),
      ),
      child: Center(
        child: SizedBox(
          height: (widget.height ?? 40) - 20,
          width: (widget.height ?? 40) - 20,
          child: CircularProgressIndicator(
            strokeWidth: 3.0,
            color: Colors.black.withAlpha(150),
          ),
        ),
      ),
    ),
  );

  Widget button() => Material(
    elevation: elevation,
    borderRadius: borderRadius,
    borderOnForeground: false,
    color: backgroundColor,
    child: InkWell(
      onTap: isActive ? onPressed : null,
      borderRadius: borderRadius,
      child: Container(
        width: widget.width,
        decoration: !haveBorder ? null : BoxDecoration(
          border: Border.all(color: borderColor),
          borderRadius: borderRadius,
        ),
        padding: widget.suffixChip != null
            ? EdgeInsets.only(left: widget.padding.left)
            : widget.padding,
        child: DefaultTextStyle.merge(
          child: content(),
          style: TextStyle(
            color: foregroundColor,
            fontSize: widget.fontSize,
          ),
        ),
      ),
    ),
  );

  Widget __stack() => Stack(
    fit: StackFit.passthrough,
    clipBehavior: Clip.none,
    alignment: AlignmentDirectional.center,
    children: <Widget>[
      if (widget.expanded)
        Positioned.fill(child: button())
      else
        button(),
      if (widget.loading)
        loadingIndicator(),
    ],
  );

  @override
  Widget build(BuildContext context) {
    Widget container = Container(
      height: widget.height,
      width: widget.width,
      margin: widget.margin,
      constraints: const BoxConstraints(
        maxHeight: 80,
      ),
      child: __stack(),
    );
    if (widget.expanded) {
      container = Expanded(child: container);
    }
    return container;
  }
}
