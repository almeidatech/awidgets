import 'package:awidgets/_internal/a_menu_mixin.dart';
import 'package:flutter/material.dart';

import 'a_button.dart';

class AButtonWithMenu extends AButton {
  final WidgetBuilder menu;
  final WidgetBuilder? menuFooter;
  final double? menuWidth, menuHeight;
  final bool persistent;

  const AButtonWithMenu({
    super.key,
    super.text,
    super.textTitle,
    super.icon,
    super.disabled = false,
    super.loading = false,
    super.color,
    super.upperCase = true,
    super.borderColor,
    super.borderRadius,
    super.fontSize = 12,
    super.iconSize = 18,
    super.fontWeight,
    super.height,
    super.width,
    super.elevation = 2,
    super.landingIcon,
    super.landingImage,
    super.trailingIcon,
    super.padding = const EdgeInsets.all(10),
    super.textColor,
    super.outlined = false,
    super.rowAlignment = MainAxisAlignment.center,
    super.singleLine = false,
    super.expanded = false,
    super.suffixChip,
    super.margin,
    required this.menu,
    this.menuFooter,
    this.persistent = false,
    this.menuWidth,
    this.menuHeight,
  }) : super(
    onPressed: null,
  );

  @override
  AButtonMenuState createState() => AButtonMenuState();
}

class AButtonMenuState extends AButtonState<AButtonWithMenu> with AMenuMixin {

  @override
  void initState() {
    super.initState();
    initMenu(
      context,
      persistent: widget.persistent,
      menu: widget.menu,
      menuFooter: widget.menuFooter,
      width: widget.menuWidth,
      height: widget.menuHeight,
      btnMargin: widget.margin,
    );
  }
}
