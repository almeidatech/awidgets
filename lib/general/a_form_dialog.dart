import 'package:awidgets/general/a_form.dart';
import 'package:flutter/material.dart';

import 'a_dialog.dart';


class AFormDialog<T> extends ADialog {
  final dynamic initialData;
  final Future<dynamic> Function()? loadInitialData;
  final List<Widget>? actions;
  final List<Widget> fields;
  final T Function(dynamic) fromJson;
  final Future<String?> Function(T) onSubmit;
  final void Function()? onSuccess;
  final bool obscureAll;
  final String? submitText;

  const AFormDialog({
    super.key,
    this.initialData,
    this.loadInitialData,
    required super.title,
    required super.persistent,
    required this.fields,
    required this.fromJson,
    required this.onSubmit,
    this.actions,
    this.onSuccess,
    this.obscureAll = false,
    super.width,
    super.titleActions,
    this.submitText,
  }): super(
    content: const <Widget>[],
    padding: 0,
    useInternalScroll: true,
  );

  @override
  AFormDialogState<T, AFormDialog<T>> createState() => AFormDialogState<T, AFormDialog<T>>();

  static Future<bool> show<T>(
      BuildContext context,
      {
        required String title,
        bool persistent = false,
        List<Widget>? titleActions,
        List<Widget>? actions,
        required List<Widget> fields,
        required T Function(dynamic) fromJson,
        required Future<String?> Function(T) onSubmit,
        void Function()? onSuccess,
        dynamic initialData,
        Future<dynamic> Function()? loadInitialData,
        bool obscureAll = false,
        double width = 400,
        String? submitText,
      }
  ) async => true == (
    await showDialog(
      context: context,
      barrierDismissible: !persistent,
      builder: (context) => AFormDialog<T>(
        obscureAll: obscureAll,
        fields: fields,
        titleActions: titleActions,
        actions: actions,
        fromJson: fromJson,
        onSubmit: onSubmit,
        onSuccess: onSuccess,
        title: title,
        persistent: persistent,
        initialData: initialData,
        loadInitialData: loadInitialData,
        width: width,
        submitText: submitText,
      ),
    )
  );

}

class AFormDialogState<T, P extends AFormDialog<T>> extends ADialogState<P> {
  bool loading = false;
  bool loadingInitialData = false;
  dynamic loadedInitialData;

  @override
  void initState() {
    super.initState();
    if (widget.loadInitialData != null) {
      loadingInitialData = true;
      loadInitialData();
    }
  }

  void loadInitialData() async {
    dynamic initialData = await widget.loadInitialData!.call();
    setState(() {
      loadedInitialData = initialData;
      loadingInitialData = false;
    });
  }

  @override
  void close(BuildContext context) {
    if (!loading) {
      super.close(context);
    }
  }

  @override
  Widget? titleWidget() => null;

  @override
  Widget content() => Flexible(
    child: loadingInitialData ? __loading() : __form(),
  );

  Widget __loading() => Padding(
    padding: const EdgeInsets.all(40),
    child: CircularProgressIndicator(
      color: Theme.of(context).primaryColor,
    ),
  );

  Widget __form() => AForm<T>(
    padding: const EdgeInsets.all(20),
    topBar: super.titleWidget(),
    obscureAll: widget.obscureAll,
    initialData: loadedInitialData ?? widget.initialData,
    fields: widget.fields,
    fromJson: widget.fromJson,
    onSubmit: widget.onSubmit,
    onSuccess: () {
      Navigator.of(context).pop(true);
      widget.onSuccess?.call();
    },
    onLoadingChange: (bool loading) {
      this.loading = loading;
    },
    submitText: widget.submitText,
    onCancelled: () async => Navigator.of(context).pop(false),
    actions: widget.actions ?? <Widget>[],
  );

}
