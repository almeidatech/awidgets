import 'package:flutter/material.dart';


class CardPosition {
  final Rect rect;
  final bool onTop;
  final bool fullHeight;

  CardPosition({
    required this.rect,
    required this.onTop,
    required this.fullHeight,
  });
}



mixin AMenuMixin {

  static const double _safePadding = 5;
  final ScrollController _scrollController = ScrollController();

  late BuildContext parentContext;
  late WidgetBuilder menu;
  WidgetBuilder? menuHeader;
  WidgetBuilder? menuFooter;
  double? width, height;
  EdgeInsets? btnMargin;
  bool persistent = false;
  double minContentHeight = 200;
  double minContentWidth = 150;

  void initMenu(
      BuildContext context,
      {
        bool persistent = false,
        required WidgetBuilder menu,
        WidgetBuilder? menuFooter,
        WidgetBuilder? menuHeader,
        double? width,
        double? height,
        EdgeInsets? btnMargin,
      }
  ) {
    parentContext = context;
    this.menu = menu;
    this.menuFooter = menuFooter;
    this.menuHeader = menuHeader;
    this.width = width;
    this.height = height;
    this.btnMargin = btnMargin;
  }

  void __close(BuildContext context) {
    if (!persistent) {
      Navigator.of(context).pop(false);
    }
  }

  CardPosition __cardPosition(BuildContext context, BoxConstraints constraints) {
    // FlutterView view = WidgetsBinding.instance.platformDispatcher.views.first;
    // FlutterView view = View.of(context);
    final Size windowSize = Size(constraints.maxWidth, constraints.maxHeight);
    final RenderBox renderBox = parentContext.findRenderObject() as RenderBox;
    final Offset offset = renderBox.localToGlobal(Offset.zero);
    final Size btnSize = renderBox.size;
    final double btnWidth = width ?? btnSize.width;
    Rect rect = Rect.fromLTWH(
      // left //
      offset.dx
          + btnSize.width / 2
          - btnWidth / 2
          + (btnMargin?.left ?? 0)
          - _safePadding,
      // top //
      offset.dy
          + btnSize.height
          - (btnMargin?.bottom ?? 0)
          - _safePadding,
      // width //
      btnWidth
          - (btnMargin?.horizontal ?? 0)
          + _safePadding*2,
      // height //
      height != null
          ? height! + _safePadding * 2
          : double.infinity,
    );
    final Rect original = rect;
    final EdgeInsets viewPadding = MediaQuery.paddingOf(context);
    final Rect safeArea = Rect.fromLTWH(
        viewPadding.left,
        viewPadding.top,
        windowSize.width - viewPadding.horizontal,
        windowSize.height - viewPadding.vertical,
    );
    rect = rect.intersect(safeArea);
    final double minWidth = minContentWidth + _safePadding * 2;
    final double minHeight = minContentHeight + _safePadding * 2;
    if (rect.width < minContentWidth) {
      for ((double, double) sides in <(double, double)>[(0.5, 0.5), (1, 0), (0, 1)]) {
        rect = Rect.fromLTRB(
          rect.left - (minWidth - rect.width) * sides.$1,
          rect.top,
          rect.right + (minWidth - rect.width) * sides.$2,
          rect.bottom,
        );
        rect = rect.intersect(safeArea);
      }
    }
    bool onTop = false;
    bool fullHeight = false;
    if (rect.height < minHeight) {
      if (safeArea.height <= minHeight) {
        fullHeight = true;
        rect = Rect.fromLTRB(
          rect.left,
          safeArea.top,
          rect.right,
          safeArea.bottom,
        );
      } else if (original.top > safeArea.height / 2) {
        // more space on top
        onTop = true;
        rect = Rect.fromLTRB(
          rect.left,
          safeArea.top,
          rect.right,
          offset.dy
              + (btnMargin?.top ?? 0)
              + _safePadding,
        );
      } else {
        // more space on bottom
        rect = Rect.fromLTRB(
          rect.left,
          original.top,
          rect.right,
          safeArea.bottom,
        );
      }
      rect = rect.intersect(safeArea);
    }
    return CardPosition(rect: rect, onTop: onTop, fullHeight: fullHeight);
  }

  Widget __container(BuildContext context, BoxConstraints constraints) {
    final CardPosition cardPosition = __cardPosition(context, constraints);
    return Stack(
      children: <Widget>[
        Positioned.fromRect(
          rect: cardPosition.rect,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: (
                cardPosition.fullHeight
                    ? MainAxisAlignment.center
                    : ( cardPosition.onTop
                        ? MainAxisAlignment.end
                        : MainAxisAlignment.start
                    )
            ),
            children: [
              Flexible(
                child: Container(
                  // avoids closing on clicks near card
                  padding: const EdgeInsets.all(_safePadding),
                  color: Colors.transparent,
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4),
                    ),
                    clipBehavior: Clip.hardEdge,
                    margin: EdgeInsets.zero,
                    elevation: 10,
                    color: Theme.of(context).colorScheme.background,
                    child:  Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[

                        if (menuHeader != null)
                          menuHeader!.call(context),

                        Flexible(child: __scrollContent(context)),

                        if (menuFooter != null)
                          menuFooter!.call(context),

                      ],
                    )
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }


  Widget __scrollContent(BuildContext context) => Scrollbar(
    controller: _scrollController,
    thumbVisibility: true,
    child: SingleChildScrollView(
      controller: _scrollController,
      child: menu.call(context),
    ),
  );

  Widget __menuContent(BuildContext context) {
    return GestureDetector(
      onTapUp: (_) => __close(context),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            return GestureDetector(
              onTapUp: (_) {/* ignore taps on card */},
              child: __container(context, constraints),
            );
          },
        ),
      ),
    );
  }

  void onPressed() {
    showDialog(
      barrierDismissible: !persistent,
      barrierColor: Colors.transparent,
      context: parentContext,
      useSafeArea: false,
      builder: (BuildContext context) => __menuContent(context),
    );
  }
}
