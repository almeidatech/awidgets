
import 'package:awidgets/general/a_table.dart';
import 'package:flutter/material.dart';

import 'general/a_button.dart';


class TestTable extends StatefulWidget {
  const TestTable({super.key});

  @override
  State<TestTable> createState() => TestTableState();
}

List<List<int>> matrix = <List<int>>[
  for (int i = 0; i < 200; ++i)
    <int>[
      for (int j = 0; j < 10; ++j)
        j+1
    ],
];

class TestTableState extends State<TestTable> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text("Testing ATable"),
      ),
      body: ATable<List<int>>(
        mobileBreakpointWidth: 800,
        columns: [
          for (int j = 0; j < matrix[0].length; ++j)
            ATableColumn(
              title: j.toString(),
              headerAlignment: TextAlign.center,
              cellBuilder: (_, __, List<int> list) => Text(
                list[j].toString() * (j+1),
                textAlign: TextAlign.center,
              ),
            ),
          ATableColumn(cellBuilder: (_, __, ___) {
            return AButton(
              icon: Icons.delete,
              color: Colors.red,
              onPressed: () async {},
            );
          }),
        ],
        loadItems: (int limit, int offset) async {
          return matrix.sublist(offset, limit+offset);
        },
      ),
    );
  }
}
