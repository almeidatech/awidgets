import 'dart:ui';

import 'package:awidgets/fields/a_drop_option.dart';
import 'package:awidgets/fields/a_field.dart';
import 'package:awidgets/fields/a_field_checkbox.dart';
import 'package:awidgets/fields/a_field_cnpj.dart';
import 'package:awidgets/fields/a_field_cpf.dart';
import 'package:awidgets/fields/a_field_cpf_cnpj.dart';
import 'package:awidgets/fields/a_field_date.dart';
import 'package:awidgets/fields/a_field_drop_down.dart';
import 'package:awidgets/fields/a_field_drop_down_lazy.dart';
import 'package:awidgets/fields/a_field_email.dart';
import 'package:awidgets/fields/a_field_money.dart';
import 'package:awidgets/fields/a_field_name.dart';
import 'package:awidgets/fields/a_field_oab.dart';
import 'package:awidgets/fields/a_field_password_confirmed.dart';
import 'package:awidgets/fields/a_field_phone.dart';
import 'package:awidgets/fields/a_field_search.dart';
import 'package:awidgets/fields/a_field_text.dart';
import 'package:awidgets/fields/a_field_text_expandable.dart';
import 'package:awidgets/general/a_button.dart';
import 'package:awidgets/general/a_menu_content.dart';
import 'package:awidgets/general/a_form.dart';
import 'package:awidgets/general/a_form_dialog.dart';
import 'package:awidgets/test_pagination.dart';
import 'package:awidgets/test_table.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      scrollBehavior: const MaterialScrollBehavior().copyWith(
        dragDevices: {
          PointerDeviceKind.mouse,
          PointerDeviceKind.touch,
          PointerDeviceKind.stylus,
          PointerDeviceKind.unknown,
        },
      ),
      title: "AWidgets test",
      theme: ThemeData(
          // colorScheme: ColorScheme.fromSeed(
          //   seedColor: Colors.deepPurple,
          //   brightness: Brightness.dark,
          // ),
          // useMaterial3: true,
          // splashColor:
          inputDecorationTheme: InputDecorationTheme(
        fillColor: Colors.grey.shade200,
      )),
      home: const MyHomePage(title: 'Flutter AWidgets Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int counter = 1;
  Key keyOab = const Key("oab");

  void testFormWithList() {
    AFormDialog.show(
      context,
      // title: "Teste de formulario com dados em lista",
      title: "Teste",
      titleActions: [
        Text("teste"),
      ],
      // initialData: {
      //   "names": <Map<String, dynamic>>[
      //     {'name': "a"},
      //     {'name': "b"},
      //     {'name': "c"},
      //   ],
      //   "money": null,
      // },
      fields: <Widget>[
        for (int i = 0; i < 5; ++i) AFieldName(identifier: 'names.$i.name'),
        AFieldMoney(identifier: 'money', label: "Valor"),
      ],
      actions: [
        AButton(onPressed: () {}, text: "Teste Antes"),
        const Spacer(),
        AButton(onPressed: () {}, text: "Teste"),
        AButton(onPressed: () {}, text: "Teste"),
        AButton(onPressed: () {}, text: "Teste"),
        AButton(onPressed: () {}, text: "Teste"),
      ],
      fromJson: (dynamic json) => json,
      onSubmit: (dynamic data) async {
        if (kDebugMode) {
          print("Received data: $data");
        }
        return "Foi?";
      },
    );
  }

  AForm testForm({required double padding}) => AForm(
        padding: EdgeInsets.all(padding),
        obscureAll: false,
        initialData: const <String, dynamic>{
          'teste': 3,
          'teste 2': 2,
        },
        fields: <AField>[
          const AFieldText(identifier: 'name', label: "Nome", required: true),
          const AFieldCPFCNPJ(identifier: 'cpf_cnpj', label: "CPF/CNPJ", required: true),
          AFieldDropDownLazy<int>(
            height: 40,
            identifier: 'teste',
            label: "Teste DropDown Lazy",
            required: true,
            searchable: false,
            loadItems: () async {
              await Future<void>.delayed(const Duration(seconds: 2));
              // throw Exception("Teste");
              return const <AOption<int>>[
                AOption<int>(label: "1", value: 1),
                AOption<int>(label: "2", value: 2),
                AOption<int>(label: "3", value: 3),
                AOption<int>(label: "4", value: 4),
              ];
            },
            onChanged: (int? value) {
              if (kDebugMode) {
                print("onChanged=$value");
              }
            },
          ),
          AFieldDropDown<int>(
            identifier: 'teste 2',
            label: "Teste DropDown",
            required: true,
            searchable: true,
            options: const <AOption<int>>[
              AOption<int>(label: "1", value: 1),
              AOption<int>(label: "2", value: 2),
              AOption<int>(label: "3", value: 3),
              AOption<int>(label: "4", value: 4),
              AOption<int>(label: "Teste do filtro", value: 5),
            ],
            onChanged: (int? value) {
              if (kDebugMode) {
                print("onChanged=$value");
              }
            },
          ),
          AFieldOAB(key: keyOab),
          const AFieldEmail(showIcon: false),
          const AFieldCPF(),
          const AFieldCNPJ(),
          AFieldDate(initialValue: DateTime.now(), required: true),
          const AFieldPhone(),
          AFieldDropDown<String>(
            label: "Opções",
            identifier: 'options',
            options: <AOption<String>>[
              AOption<String>(label: "Foo", value: 'foo'),
              AOption<String>(label: "BAR", value: 'bar'),
            ],
            initialValue: 'foo',
            searchable: true,
            // searchable: true,
          ),
          // FilterDropDown(
          //   label: "Opções",
          //   emptyText: "Nenhum Encontrado",
          //   identifier: 'options',
          //   options: <AOption>[
          //     AOption(label: "Foo", value: 'foo'),
          //     AOption(label: "BAR", value: 'bar'),
          //   ],
          //   initialValue: 'foo',
          //   // searchable: true,
          // ),
          // ADrop
        ],
        fromJson: (dynamic json) => json,
        // actions: <AAction>[
        //   AActionCancel(onAction: (){}),
        // ],
        onSubmit: (dynamic json) async {
          if (kDebugMode) {
            print("Recebido: $json");
            print("Aguardando um tempo");
            await Future.delayed(const Duration(seconds: 2));
            print("Submetido??");
          }
          return null;
        },
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
          title: Text(widget.title),
        ),
        body: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              const Text('You have pushed the button this many times:'),
              Text(
                '$counter',
                style: Theme.of(context).textTheme.headlineMedium,
              ),
              AButton(
                text: "Teste",
                height: 50,
                onPressed: () {},
              ),
              const SizedBox(height: 10),
              SizedBox(
                width: 400,
                child: AFieldTextExpandable(
                  identifier: 'notes',
                  label: 'Anotações',
                  timerTime: 800,
                  maxHeight: 150,
                  onChanged: (String? value) async {
                    String? message;
                    try {} finally {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text(
                              message ?? 'Anotações atualizadas com sucesso'),
                          backgroundColor: Colors.green,
                          duration: const Duration(seconds: 2),
                        ),
                      );
                    }
                  },
                ),
              ),
              const SizedBox(height: 10),
              SizedBox(
                width: 200,
                child: AButton(
                  text: "Teste de Botao com um texto muito longo",
                  singleLine: true,
                  trailingIcon: Icons.add,
                  onPressed: () {},
                ),
              ),
              AButtonWithMenu(
                text: "T",
                upperCase: false,
                margin: const EdgeInsets.only(top: 100, left: 800, bottom: 40),
                // menuHeight: 100,
                menu: (BuildContext context) => Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const Text("teste"),
                    const Text("teste"),
                    const Text("teste"),
                    const Text("teste"),
                    const Text("teste"),
                    const Text("teste"),
                    AButton(
                      text: "Botao",
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 10),
              AButton(
                text: "Teste AForm com lista",
                onPressed: testFormWithList,
              ),
              const SizedBox(height: 10),
              AButton(
                text: "Teste ATable",
                onPressed: () => Navigator.of(context).push(
                  MaterialPageRoute(builder: (_) => const TestTable()),
                ),
              ),
              const SizedBox(height: 10),
              SizedBox(
                width: 800,
                child: Wrap(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  // mainAxisSize: MainAxisSize.min,
                  alignment: WrapAlignment.center,
                  runAlignment: WrapAlignment.center,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: <Widget>[
                    AButton(
                      // height: 50,
                      text: "Vinicius Almeida dos Santos",
                      textTitle: "Responsável Principal",
                      upperCase: false,
                      // outlined: true,
                      // loading: true,
                      onPressed: () {
                        setState(() {
                          ++counter;
                        });
                      },
                      // width: 300,
                      // expanded: true,
                    ),
                    const SizedBox(width: 10),
                    AButton(
                      text: "Abrir dialog",
                      outlined: true,
                      onPressed: () {
                        AFormDialog.show(
                          context,
                          loadInitialData: () async {
                            await Future<void>.delayed(
                                const Duration(seconds: 1));
                            return <String, dynamic>{
                              'email': 'teste',
                              'active': true,
                            };
                          },
                          title: "Teste de dialog",
                          obscureAll: false,
                          persistent: true,
                          fields: const <AField>[
                            AFieldEmail(required: true),
                            AFieldPasswordConfirmed(),
                            AFieldCheckbox(
                                identifier: 'active', label: "check"),
                          ],
                          fromJson: (dynamic json) => json,
                          // actions: <AAction>[
                          //   AActionCancel(onAction: (){}),
                          // ],
                          onSubmit: (dynamic json) async {
                            await Future.delayed(const Duration(seconds: 5));
                            if (kDebugMode) {
                              print("Recebido: $json");
                              print("Aguardando um tempo");
                              print("Submetido??");
                            }
                            return "Erro";
                          },
                        );
                      },
                    ),
                    const SizedBox(width: 10),
                    AButton(
                      text: "Com chip",
                      onPressed: () {},
                      suffixChip: const Text("1 / 2"),
                    ),
                    AButton(
                      color: Colors.grey.shade200,
                      height: 50,
                      text: "Com chip",
                      onPressed: () {},
                      suffixChip: const Text("1 / 2"),
                    ),
                    const SizedBox(width: 10),
                    AButton(
                      onPressed: () {},
                      icon: Icons.home,
                    ),
                  ],
                ),
              ),
              AFieldSearch(onChanged: (String? value) {}),
              testForm(padding: 50),
              AButton(
                text: "Open pagination test",
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => const TestPagination(),
                    ),
                  );
                },
              ),
            ],
          ),
        ));
  }
}
